
class StopWatchManager: ObservableObject {
    enum StopWatchModes {
        case running
        case stopped
        case paused
    }
    
    enum TimingModes: Double {
        case straight = 10
        case boomerang = 5
    }
    
    private var timer: Timer? = nil
    @Published var secondsElapsed = 0.0
    @Published var mode: StopWatchModes = .stopped
    @Published var isDone = false
    
    func start(timingMode: TimingModes = .straight) {
        mode = .running
        timer = Timer.scheduledTimer(
            withTimeInterval: 1,
            repeats: true
        ) { timer in
            self.secondsElapsed += 1
            
            if self.secondsElapsed >= timingMode.rawValue {
                self.isDone = true
            }
        }
        RunLoop.current.add(timer!, forMode: .common)
    }
    
    func stop() {
        guard timer != nil else {
            return
        }
        timer!.invalidate()
        timer = nil
        secondsElapsed = 0
        mode = .stopped
        isDone = false
    }
}
