//
//  ImageUtility.swift
//  react-native-tokkan-ar
//
//  Created by glen rickard on 2021-01-11.
//

import VideoToolbox

enum ImageTypes {
    case JPEG
    case PNG
}

class ImageUtility {
    private init() {}
    
    static func cachesPath() -> URL? {
        let path = FileManager.default.urls(
            for: .cachesDirectory,
            in: .userDomainMask
        )
        return path.first
    }
    
    static func loadImageByUrl(url: URL) -> UIImage? {
        do {
            let imageData = try Data(contentsOf: url)
            return UIImage(data: imageData)
        } catch {
            return nil
        }
    }
    
    static func deleteImage(url: URL) -> Void {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print("Failed to delete image at: \(url.absoluteString)")
        }
    }
    
    static func saveImageToPath(from path: String, to newPath: String) {
    }
    
    static func saveAsJPEG(
        _ image: UIImage,
        compressionQuality: CGFloat = 0.5
    ) -> URL? {
        let ext = ".jpeg"
        let uuid = UUID().uuidString
//        if let jpgData = image.jpegData(
//            compressionQuality: compressionQuality
//        ),
//        let path = cachesPath()?.appendingPathComponent("roamr-\(uuid)\(ext)") {
//            try? jpgData.write(to: path)
//            return path
//        }
//        return nil
        guard let jpgData = image.jpegData(compressionQuality: compressionQuality) else {
            return nil
        }
        guard let path = cachesPath()?
                .appendingPathComponent("roamr-\(uuid)\(ext)") else {
            return nil
        }
        try? jpgData.write(to: path)
        return path
    }
    
    static func bufferToUIImage(
//        buffer: CVBuffer
        buffer: CVPixelBuffer
    ) -> UIImage? {
        var cgImage: CGImage?
        VTCreateCGImageFromCVPixelBuffer(
            buffer,
            options: nil,
            imageOut: &cgImage
        )
        
        guard let cgImage2 = cgImage else {
            return nil
        }
        
        let image = UIImage(cgImage: cgImage2)
        return image
    }
    
    static func bufferToFile(
        _ buffer: CVBuffer,
        imageType: ImageTypes = ImageTypes.JPEG
    ) -> URL? {
//        switch imageType {
//        case .JPEG:
//            bufferToUIImage(buffer: buffer)
//        case .PNG:
//        }
        guard let uiImage = bufferToUIImage(buffer: buffer) else {
            return nil
        }
        return saveAsJPEG(uiImage)
    }
    
    static func uiImageToFile(
        _ image: UIImage
    ) -> URL? {
        return saveAsJPEG(image)
    }
}
