//
//  ARPreviewVideoView.swift
//  react-native-tokkan-ar
//
//  Created by glen rickard on 2021-01-13.
//

import SwiftUI
//import AVFoundation
import AVKit

@available(iOS 14.0, *)
struct ARPreviewVideoView: View {
    private let player = AVPlayer(url: URL(fileURLWithPath: ""))
    
    init() {
        player.preventsDisplaySleepDuringVideoPlayback = true
    }
    
    var body: some View {
        VideoPlayer(player: player, videoOverlay: {
            EmptyView()
        })
        .onAppear() {
            player.play()
        }
    }
}
