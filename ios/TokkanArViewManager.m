#import <Foundation/Foundation.h>
#import <React/RCTViewManager.h>
//#import "ARCaptureViewProxy.swift"
//#import "ARCaptureViewManager.swift"
//#import "ARCaptureView.swift"
//#import "ARCaptureView.m"
//#import "react-native-tokkan-ar-Swift.h"
//#import "TokkanArExample-Swift.h"
//#import "TokkanAr-Swift.h"

@interface TokkanArViewManager : RCTViewManager
@end

@implementation TokkanArViewManager

RCT_EXPORT_MODULE(TokkanArView)

- (UIView *)view
{
  return [[UIView alloc] init];
}

RCT_CUSTOM_VIEW_PROPERTY(color, NSString, UIView)
{
  [view setBackgroundColor:[self hexStringToColor:json]];
}

- hexStringToColor:(NSString *)stringToConvert
{
  NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""];
  NSScanner *stringScanner = [NSScanner scannerWithString:noHashString];

  unsigned hex;
  if (![stringScanner scanHexInt:&hex]) return nil;
  int r = (hex >> 16) & 0xFF;
  int g = (hex >> 8) & 0xFF;
  int b = (hex) & 0xFF;

  return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f];
}

@end
