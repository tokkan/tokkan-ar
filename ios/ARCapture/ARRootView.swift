import SwiftUI

enum ARCaptureScreens: String {
    case capture
    case imagePreview
    case videoPreview
}

class ViewRouter: ObservableObject {
    @Published var selectedScreen: ARCaptureScreens = .capture
}

struct ARRootView: View {
    @StateObject var viewRouter = ViewRouter()
    @State private var requestSnap: Bool = false
    @State private var startRecording: Bool = false
    @State private var stopRecording: Bool = false
    @State private var openFaceFilterMenu: Bool = false
    @State private var arResult: ARCaptureResult? = nil
    
    func onNavigate(_ toScreen: ARCaptureScreens) {
        print("onNavigate")
        requestSnap = false
        viewRouter.selectedScreen = toScreen
    }
    
    var body: some View {
        NavigationView {
            EmptyView()
            switch viewRouter.selectedScreen {
            case .capture:
                ARCaptureView(
                    requestSnap: $requestSnap,
                    startRecording: $startRecording,
                    stopRecording: $stopRecording,
                    openFaceFilterMenu: $openFaceFilterMenu,
                    arResult: $arResult,
                    onNavigate: onNavigate(_:)
                )
                .environmentObject(viewRouter)
                .navigationTitle("")
                .navigationBarTitle("")
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                .edgesIgnoringSafeArea(.all)
            case .imagePreview, .videoPreview:
                ARPreviewImageView(
                    arResult: $arResult,
                    onNavigate: onNavigate(_:)
                )
                .environmentObject(viewRouter)
                .navigationTitle("")
                .navigationBarTitle("")
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                .edgesIgnoringSafeArea(.all)
            }
        }
        .navigationTitle("")
        .navigationBarTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
    }
}

struct ARRootView_Previews: PreviewProvider {
    static var previews: some View {
        ARRootView()
    }
}
