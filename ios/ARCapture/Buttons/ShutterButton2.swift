import SwiftUI

struct ShutterButton2: View {
    enum EventState {
        case inactive
        case tap
        case longPress
        case drag
    }
    
    enum TouchState {
        case tap
        case longPress
        case drag
        case stop
    }
    
    enum ShutterState {
        case snap
        case record
        case continueRecording
        case stopRecord
    }
    
    enum ShutterActions {
        case snap
        case record
        case stopRecord
    }
    
    @State private var isPressed = false
    @GestureState private var isLongPress = false
    @State private var isDoubleTap = false
    @State private var touchState: TouchState = .tap
    @State private var shutterState: ShutterState = .snap
    @GestureState private var isTap = false
    @GestureState private var eventState = EventState.inactive
//    let changeState: (EventAction) -> Void
    
    func trigger(action: ShutterActions) {
        switch action {
        case .snap:
            print("trigger snap")
        case .record:
            print("trigger record")
        case .stopRecord:
            print("trigger stop record")
        }
    }
    
    var body: some View {
        let gestureInitializer = LongPressGesture(minimumDuration: 0.25)
            .updating($isLongPress) { currentState, gestureState, transaction in
                print("Updating in gestureInitializer")
                gestureState = currentState
            }
            .onEnded { value in
                print("onEnded in init")
                print(eventState)
                print(value)
            }
        
        let shortPressGesture = LongPressGesture(minimumDuration: 0)
            .updating($isTap) { value, state, transaction in
                print("updating in shortpress")
            }
            .onEnded { value in
                print("Short press goes here")
                print(eventState)
                print(value)
                isPressed = value
                
                switch shutterState {
                case .snap:
                    print("capture snap")
                    trigger(action: .snap)
                default:
                    print("don't capture snap")
                }
            }
        
        let longPressOrDragGesture = LongPressGesture(minimumDuration: 1.0)
            .sequenced(before: DragGesture())
            .updating($eventState) { value, state, transaction in
                switch value {
                
                // Long press begins.
                case .first(true):
                    print("pressing")
                    state = .longPress
//                    touchState = .longPress
                    shutterState = .record
                    
                // Long press confirmed, dragging may begin.
                case .second(true, let drag):
                    // state = .dragging(translation: drag?.translation ?? .zero)
                    print("dragging")
                    print(drag?.translation ?? .zero)
                    state = .drag
//                    touchState = .drag
                    shutterState = .record
                    
                // Dragging ended or the long press cancelled.
                default:
                    print("Dragging ended or long press cancelled")
                    print("Cancel recording")
                    state = .inactive
                }
            }
            .onEnded { value in
                print("long or drag onEnded")
                guard case .second(true, let drag?) = value else {
                    print("guard onEnded")
                    return
                }
                
                print("onEnded in X1")
                print(drag.translation)
                
                if drag.translation.width < 10.0 &&
                    drag.translation.width > -10.0 {
                    if drag.translation.height > 45.0 {
                        print("dragging")
                        touchState = .drag
                        shutterState = .continueRecording
                    } else {
                        print ("cancel long press and stop recording")
                        touchState = .stop
                        shutterState = .stopRecord
                    }
                }
            }
        
        let combinedGesture = gestureInitializer
            .sequenced(before: longPressOrDragGesture)
            .exclusively(before: shortPressGesture)
        
        Circle()
            .opacity(0)
            .frame(width: 75, height: 75, alignment: .center)
            .background(Circle()
                            .stroke(lineWidth: 4)
                            .fill(isLongPress ? Color.red : Color.white))
            .contentShape(Circle())
            .scaleEffect(isLongPress ? 1.05: 1)
            .animation(.spring())
            .gesture(combinedGesture)
            .onChange(of: eventState, perform: { value in
                print("onChange eventState")
                print(value)
                
                switch value {
                case .longPress:
                    trigger(action: .record)
                case .inactive:
                    trigger(action: .stopRecord)
                default:
                    return
                }
            })
            .onChange(of: isTap, perform: { value in
                print("onChange isTap")
                print(value)
            })
//            .onTapLongPressDragEvent(changeState: { (buttonState) in
//                print("from event modifier")
//                print(buttonState)
//
//                switch buttonState {
//                default:
//                    return
//                }
//            })
    }
}

//struct ShutterButton: View {
//    enum DragState {
//        case inactive
//        case pressing
//        case dragging
//    }
//
//    @State var viewState = CGSize.zero
//    @State private var isPressed = false
//    @State private var isLongPress = false
//    @State private var isDoubleTap = false
//    @State private var isActivated = false
//
//    @GestureState var longPress = false
//    @GestureState var longDrag = false
//    @GestureState var dragState = DragState.inactive
//
//    func trigger(action: DragState) {
//        switch action {
//        case .inactive:
//            print("inactive")
//        case .pressing:
//            print("pressing")
//        case .dragging:
//            print("dragging")
//        }
//    }
//
//    var body: some View {
//        let longPressGestureDelay = DragGesture(minimumDistance: 0)
//            .updating($longDrag) { currentstate, gestureState, transaction in
//                gestureState = true
//            }
//            .onEnded { value in
//                print(value.translation) // We can use value.translation to see how far away our finger moved and accordingly cancel the action (code not shown here)
//                print("long press action goes here")
//
//                if value.translation.width < 10.0 &&
//                    value.translation.width > -10.0 {
//                    if value.translation.height > 45.0 {
//                        print("dragging")
//                    } else {
//                        print ("long press")
//                    }
//                }
//            }
//
//        let shortPressGesture = LongPressGesture(minimumDuration: 0)
//            .onEnded { _ in
//                print("short press goes here")
//            }
//
//        let longTapGesture = LongPressGesture(minimumDuration: 0.25)
//            .updating($longPress) { currentstate, gestureState, transaction in
//                gestureState = true
//            }
//            .onEnded { _ in
//                print("onEnded")
//            }
//
//        let tapBeforeLongGestures = longTapGesture.sequenced(before:longPressGestureDelay).exclusively(before: shortPressGesture)
//
//        Circle()
//            .opacity(0)
//            .frame(width: 75, height: 75, alignment: .center)
//            .background(Circle()
//                            .stroke(lineWidth: 4)
//                            .fill(isPressed ? Color.red : Color.white))
//            .contentShape(Circle())
//            .scaleEffect(isPressed ? 1.05: 1)
//            .animation(.spring())
//            .gesture(tapBeforeLongGestures)
////            .modifier(TouchDownUpEventModifier(changeState: { (buttonState) in
////                if buttonState == .pressed {
////                    isPressed = true
////                } else {
////                    isPressed = false
////                }
////            }))
//    }
//}
