import SwiftUI

struct ShutterButton: View {
    private enum EventState {
        case inactive
        case pressing
        case outOfBounds
        
        var isPressed: Bool {
            switch self {
            case .pressing:
                return true
            default:
                return false
            }
        }
    }
    
    private enum ShutterActions {
        case snap
        case record
        case stopRecord
    }
    
    @ObservedObject private var stopWatchManager = StopWatchManager()
    @Binding var requestSnap: Bool
    @State private var isPressed = false
    @State private var isRecording = false
    @State private var handsFreeRecording = false
    @GestureState private var eventState: EventState = .inactive
    
    private func trigger(action: ShutterActions) {
        switch action {
        case .snap:
            print("trigger snap")
            requestSnap = true
        case .record:
            print("trigger record")
            stopWatchManager.start()
            isRecording = true
        case .stopRecord:
            print("trigger stop record")
            if isRecording {
                stopWatchManager.stop()
            }
            handsFreeRecording = false
            isRecording = false
        }
    }
        
    var body: some View {
        let tapGesture = LongPressGesture(minimumDuration: 0)
            .onEnded { value in
                print("TapGesture onEnded | value: \(value)")
                trigger(action: .snap)
            }
        
        let longPressGesture = LongPressGesture(minimumDuration: 0.25)
            .onEnded { value in
                print("LongPressGesture onEnded | value: \(value)")
                trigger(action: .record)
            }
        
        let dragGesture = DragGesture(minimumDistance: 0)
//        let dragGesture = LongPressGesture(minimumDuration: 1)
//            .sequenced(before: DragGesture(minimumDistance: 0))
            .updating($eventState) { value, state, transaction in
//                print("DragGesture updating | value: \(value) | state: \(state)")
//                print("DragGesture updating")
//                print(stopWatchManager.secondsElapsed)
//                print(stopWatchManager.mode)
//                print(stopWatchManager.secondsElapsed)
//                print(stopWatchManager.isDone)
                
                if stopWatchManager.isDone {
                    print("Recording time has run out")
                    trigger(action: .stopRecord)
                }
            }
            .onEnded { value in
                print("DragGesture onEnded | value: \(value.translation) | location: \(value.location)")
//                guard case .second(true, let drag?) = value else {
//                    print("guard")
//                    return
//                }
                let width = value.location.x
                let height = value.location.y
                
                if width < 80.0 &&
                    width > -10.0 {
                    if height > 80.0 {
                        print("caps recording")
                        self.handsFreeRecording = true
                    } else if height < -40 {
                        print ("cancel long press and stop recording")
                        if (!self.handsFreeRecording) {
                            trigger(action: .stopRecord)
                        }
                    }
                }
            }
        
        let combinedGesture = longPressGesture
            .sequenced(before: dragGesture)
            .exclusively(before: tapGesture)

        Circle()
            .opacity(0)
            .frame(width: 75, height: 75, alignment: .center)
            .background(Circle()
                            .stroke(lineWidth: 4)
                            .fill(isPressed ? Color.red : Color.white))
            .contentShape(Circle())
            .scaleEffect(isPressed ? 1.05: 1)
            .animation(.spring())
            .gesture(combinedGesture)
    }
}
