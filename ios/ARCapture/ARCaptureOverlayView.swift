import SwiftUI

struct FaceFilter {
    var id: String = ""
    var name: String = ""
}

struct ARCaptureOverlayView: View {
    @State var faceFilters = [FaceFilter]()
    @Binding var requestSnap: Bool
    @Binding var openFaceFilterMenu: Bool
    let onBackAction: (_ screen: ARCaptureScreens) -> Void
    
    func onBack() {
        onBackAction(.capture)
    }
    
    let dragGesture = DragGesture()
        .onChanged({
            e in print("On drag onChanged")
            print(e)
        })
        .onEnded({
            e in print("On drag onEnded")
            print(e)
        })
    
    let tapGesture = TapGesture()
        .onEnded({
            e in
            print("On tap onEnded")
            print(e)
        })
    
//    let longPressGesture = LongPressGesture()
//        .onChanged({
//            e in print("On longpress onChanged")
//            print(e)
//        })
//        .onEnded({
//            e in print("On longpress onEnded")
//            print(e)
//        })
    
    let magnificationGesture = MagnificationGesture()
        .onChanged({
            e in print("On magnification onChanged")
            print(e)
        })
        .onEnded({
            e in print("On magnification onEnded")
            print(e)
        })
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
//                OverlayHeader()
                TopBarHeader(
                    onBackAction: onBack,
                    centerView: nil,
                    rightView: nil
                )
                Spacer()
                OverlayBottom(
                    requestSnap: self.$requestSnap,
                    openFaceFilterMenu: self.$openFaceFilterMenu
                )
            }
            Spacer()
        }
    }
}

//struct ARCaptureOverlayView_Previews: PreviewProvider {
//    static var previews: some View {
//        ARCaptureOverlayView(requestSnap: false)
//    }
//}

// MARK: - Right toolbar

struct OverlayCaptureToolbar: View {
    @Binding var isFlashOn: Bool
    
    var body: some View {
        VStack {
            Button(action: {
                print("Edit toolbar 1")
            }) {
                Image(systemName: isFlashOn ? "bolt.fill" : "bolt.slash")
                    .padding()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.white)
            }
            .buttonStyle(ToolbarButtonStyle())
            Button(action: {
                print("Edit toolbar 2")
            }) {
                Image(systemName: "camera.filters")
                    .padding()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.white)
            }
            .buttonStyle(ToolbarButtonStyle())
            Button(action: {
                print("Edit toolbar 3")
            }) {
                Image(systemName: "textformat")
                    .padding()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.white)
            }
            .buttonStyle(ToolbarButtonStyle())
        }
    }
}

struct OverlayPreviewToolbar: View {
    var body: some View {
        VStack {
            Button(action: {
                print("Edit toolbar 1")
            }) {
                Image(systemName: "scribble.variable")
                    .padding()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.white)
            }
            .buttonStyle(ToolbarButtonStyle())
            Button(action: {
                print("Edit toolbar 2")
            }) {
                Image(systemName: "camera.filters")
                    .padding()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.white)
            }
            .buttonStyle(ToolbarButtonStyle())
            Button(action: {
                print("Edit toolbar 3")
            }) {
                Image(systemName: "textformat")
                    .padding()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.white)
            }
            .buttonStyle(ToolbarButtonStyle())
        }
    }
}

// MARK: - Bottom

struct OverlayBottom: View {
    @State private var isLongPress = false
    @State private var isDoubleTap = false
    @Binding var requestSnap: Bool
    @Binding var openFaceFilterMenu: Bool
    
    var body: some View {
//        VStack(alignment: .center) {
            HStack(alignment: .center) {
//                Spacer()
//                EmptyView()
                Spacer()
                    .frame(width: 50, height: 50, alignment: .center)
               ShutterButton(requestSnap: $requestSnap)
                
                Button("", action: {
                    print("Clicked face filter")
                    self.openFaceFilterMenu = true
                }).buttonStyle(FaceFilterButtonStyle())
                .onTapGesture(count: 1) {
                    print("On tap shutter button")
                }
                .onLongPressGesture {
                    print("On long press shutter button")
                }
            }
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: 100,
                   alignment: .center
            )
            .padding(25)
//        }
    }
}

struct OverlayBottomFaceFilters: View {
    var body: some View {
        HStack(alignment: .center) {
            
        }
    }
}
