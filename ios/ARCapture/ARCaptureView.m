//
//  ARCaptureView.m
//  TokkanAr
//
//  Created by glen rickard on 2021-01-07.
//  Copyright © 2021 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTViewManager.h>
//#import "react-native-tokkan-ar-Swift.h"
#import "react_native_tokkan_ar-Swift.h"

//@interface RCT_EXTERN_MODULE(ARCaptureViewManager, RCTViewManager)
//@interface RCT_EXTERN_MODULE(ARCaptureViewProxy, RCTViewManager)

//@end

@interface ARCaptureViewManager: RCTViewManager
@end

@implementation ARCaptureViewManager

RCT_EXPORT_MODULE()

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

- (UIView *)view
{
//  return [[UIView alloc] init];
    ARCaptureViewProxy *proxy = [[ARCaptureViewProxy alloc] init];
    UIView *view = [proxy view];
    return view;
}

@end
