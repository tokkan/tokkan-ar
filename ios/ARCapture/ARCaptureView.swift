import SwiftUI

struct ARCaptureView: View {
    @EnvironmentObject var viewRouter: ViewRouter
//    @State private var requestSnap: Bool = false
//    @State private var startRecording: Bool = false
//    @State private var stopRecording: Bool = false
//    @State private var openFaceFilterMenu: Bool = false
    @Binding var requestSnap: Bool
    @Binding var startRecording: Bool
    @Binding var stopRecording: Bool
    @Binding var openFaceFilterMenu: Bool
    @Binding var arResult: ARCaptureResult?
    
    let onNavigate: (_ screen: ARCaptureScreens) -> Void
    
    func onCaptureImage() -> Void {
        requestSnap = false
    }
    
    var body: some View {
        ARCaptureViewControllerRepresentable(
            requestSnap: self.$requestSnap,
            startRecording: self.$startRecording,
            stopRecording: self.$stopRecording,
            openFaceFilterMenu: self.$openFaceFilterMenu,
            arResult: self.$arResult,
            onNavigate: onNavigate,
            onCaptureImage: onCaptureImage
        )
        .overlay(ARCaptureOverlayView(
            requestSnap: self.$requestSnap,
            openFaceFilterMenu: self.$openFaceFilterMenu,
            onBackAction: onNavigate
        ))
    }
}

//struct ARCaptureView_Previews: PreviewProvider {
//    static var previews: some View {
//        ARCaptureView(arResult: )
//    }
//}

struct ARCaptureViewControllerRepresentable: UIViewControllerRepresentable {
    @Binding var requestSnap: Bool
    @Binding var startRecording: Bool
    @Binding var stopRecording: Bool
    @Binding var openFaceFilterMenu: Bool
    @Binding var arResult: ARCaptureResult?
    let onNavigate: (_ screen: ARCaptureScreens) -> Void
    let onCaptureImage: () -> Void
    
    typealias UIViewControllerType = ARCaptureViewController
    
    func makeUIViewController(context: Context) -> ARCaptureViewController {
        let controller = ARCaptureViewController()
        return controller
    }
    
    func updateUIViewController(_ uiViewController: ARCaptureViewController, context: Context) {
        print("updateUIViewController")
        print("arResult: \(self.arResult?.url.debugDescription ?? "no arResult")")
        
        if (self.requestSnap) {
            print("Request snap in representable")
//            requestSnap = false
            guard let result = uiViewController.captureImage() else {
//            guard let result = uiViewController.captureFrame() else {
                print("Failed to capture image")
                return
            }
            arResult = result
            print("Result: \(result)")
            //            self.arResult = result
            
            print("result: \(result.url.debugDescription)")
            print("arResult: \(self.arResult?.url.debugDescription ?? "no arResult")")
            onCaptureImage()
            onNavigate(.imagePreview)
        }
        
        if (self.startRecording) {
            print("Starting recording")
            self.startRecording = false
        }
        
        if (self.openFaceFilterMenu) {
//            openFaceFilterMenu = false
            uiViewController.changeFaceFilter()
        }
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(
            requestSnap: $requestSnap,
            startRecording: $startRecording,
            stopRecording: $stopRecording,
            arResult: $arResult
        )
    }
    
    class Coordinator: NSObject {
        @Binding var requestSnap: Bool
        @Binding var startRecording: Bool
        @Binding var stopRecording: Bool
        @Binding var arResult: ARCaptureResult?
        
        init(
            requestSnap: Binding<Bool>,
            startRecording: Binding<Bool>,
            stopRecording: Binding<Bool>,
            arResult: Binding<ARCaptureResult?>
        ) {
            _requestSnap = requestSnap
            _startRecording = startRecording
            _stopRecording = stopRecording
            _arResult = arResult
        }
    }
}
