//
//  ARCaptureViewProxy.swift
//  react-native-tokkan-ar
//
//  Created by glen rickard on 2021-01-09.
//

import SwiftUI

@objcMembers open class ARCaptureViewProxy: NSObject {
//    private var vc = UIHostingController(rootView: ARCaptureView())
    private var vc = UIHostingController(rootView: ARRootView())
    public var view: UIView {
        return vc.view
    }
}
