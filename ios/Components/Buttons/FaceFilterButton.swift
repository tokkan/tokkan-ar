//
//  FaceFilterButton.swift
//  react-native-tokkan-ar
//
//  Created by glen rickard on 2021-01-12.
//

import SwiftUI

struct FaceFilterButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(width: 35, height: 35, alignment: .center)
            .background(Circle()
                            .stroke(lineWidth: 4)
                            .fill(configuration.isPressed ? Color.green : Color.white))
            .contentShape(Circle())
            .scaleEffect(configuration.isPressed ? 1.05: 1)
            .animation(.spring())
        //            .position(x: 50, y: 10)
    }
}
