//
//  ToolbarButton.swift
//  react-native-tokkan-ar
//
//  Created by glen rickard on 2021-01-13.
//

import SwiftUI

struct ToolbarButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(width: 20, height: 20, alignment: .center)
            .background(Circle()
                            .stroke(lineWidth: 1)
                            .fill(configuration.isPressed ? Color.green : Color.white))
            .contentShape(Circle())
            .scaleEffect(configuration.isPressed ? 1.05: 1)
            .animation(.spring())
    }
}
