//
//  TapLongPressDragViewModifier.swift
//  react-native-tokkan-ar
//
//  Created by glen rickard on 2021-01-16.
//
// Usage:
//  Circle()
//    .opacity(0)
//    .frame(width: 75, height: 75, alignment: .center)
//    .background(Circle()
//                    .stroke(lineWidth: 4)
//                    .fill(isPressed ? Color.red : Color.white))
//    .contentShape(Circle())
//    .scaleEffect(isPressed ? 1.05: 1)
//    .animation(.spring())
//    .onTapLongPressDragEvent(changeState: { (buttonState) in
//        print("from event modifier")
//        print(buttonState)
//
//        switch buttonState {
//        default:
//            return
//        }
//    })
//
// TODO:
// Complex gestures that needs start stop over three different gestures
// Dragging on ended is needed in onEnded where state as in the updating function isn't available. Also tap gestures needs to be handled in conjunction with LongPress gestures, which isn't a straightforward thing to do.

import SwiftUI

//enum ShutterButtonState {
//    case pressed
//    case notPressed
//}

//enum ShutterButtonAction {
//    case tap
//    case doubleTap
//    case longPress
//    case drag
//}

//struct TouchDownUpEventModifier: ViewModifier {
//    @GestureState private var isPressed = false
//    let changeState: (ShutterButtonState) -> Void
//
//    func body(content: Content) -> some View {
//        let drag = DragGesture(minimumDistance: 0)
//
//            /// this is called whenever the gesture is happening
//            /// because we do this on a `DragGesture`, this is called when the finger is down
//            .updating($isPressed) { (value, gestureState, transaction) in
//
//                /// setting the gestureState will automatically set `$isPressed`
//                gestureState = true
//            }
//
//        return content
//            .gesture(drag) /// add the gesture
//            .onChange(of: isPressed, perform: { (pressed) in /// call `changeState` whenever the state changes
//                /// `onChange` is available in iOS 14 and higher.
//                print("onChange in event modifier")
//                if pressed {
//                    print("isPressed")
//                    self.changeState(.pressed)
//                } else {
//                    print("notPressed")
//                    self.changeState(.notPressed)
//                }
//            })
//    }
//}

public enum EventAction {
    case tap
    case long
    case dragOut
    case cancelled
}

struct TapLongPressDragEventModifier: ViewModifier {
    enum EventState {
        case inactive
        case tap
        case pressing
        case dragging
        case dragOut
        case stop
    }
    
    @State var eventAction = EventAction.cancelled
    @GestureState private var isPressed = false
    @GestureState private var isTap = true
    @GestureState private var isLongPress = false
    @GestureState private var eventState = EventState.inactive
    let changeState: (EventAction) -> Void
    
    func body(content: Content) -> some View {
        let gestureInitializer = LongPressGesture(minimumDuration: 0.25)
//        let gestureInitializer = LongPressGesture(minimumDuration: 1.0)
            .updating($isLongPress) { currentstate, gestureState, transaction in
                print("updating in gestureInitializer")
                gestureState = true
            }
//            .updating($eventState) { value, state, transaction in
//                print("updating in gestureInitializer")
//                state = .tap
//            }
            .onChanged { _ in
                print("onChanged in initialization")
            }
            .onEnded { _ in
                print("onEnded in initialization")
            }
        
        let shortPressGesture = LongPressGesture(minimumDuration: 0)
        //        let shortPressGesture = DragGesture(minimumDistance: 0)
//        let shortPressGesture = LongPressGesture(minimumDuration: 0.1, maximumDistance: 0)
            .sequenced(before: DragGesture())
//            .updating($eventState) { value, state, transaction in
//                state = .tap
//                print("updating in tap")
//                print(value)
//            }
            .updating($isLongPress) { value, state, transaction in
                print("updating short press")
                state = true
            }
            .onEnded { value in
                print("short press onEnded")
                print(value)
                print(self.eventAction)
                print(self.eventState)
                print(self.isLongPress)
                
                switch self.eventAction {
                case .tap:
                    print("short press goes here")
                case .long:
                    print("long press touch up/out")
                default:
                    print("short press onEnded default")
                    return
                }
                
                eventAction = .tap
            }
        
        let minimumLongPressDuration = 1.3
        let longPressOrDragGesture = LongPressGesture(minimumDuration: minimumLongPressDuration)
            .sequenced(before: DragGesture())
            .updating($eventState) { value, state, transaction in
                switch value {
                
                // Long press begins.
                case .first(true):
                    print("pressing")
                    state = .pressing
                    
                // Long press confirmed, dragging may begin.
                case .second(true, let drag):
                    // state = .dragging(translation: drag?.translation ?? .zero)
                    print("dragging")
                    print(drag?.translation ?? .zero)
                    state = .dragging
                    
                // Dragging ended or the long press cancelled.
                default:
                    print("Dragging ended or long press cancelled")
                    print("Cancel recording")
                    state = .inactive
                }
            }
            .onEnded { value in
                guard case .second(true, let drag?) = value else {
                    print("guard onEnded")
                    return
                }
                
                print("onEnded in X1")
                print(drag.translation)
                
                if drag.translation.width < 10.0 &&
                    drag.translation.width > -10.0 {
                    if drag.translation.height > 45.0 {
                        print("dragging")
                    } else {
                        print ("cancel long press and stop recording")
                    }
                }
            }
        
        let combinedGesture = gestureInitializer
            .sequenced(before: longPressOrDragGesture)
            .exclusively(before: shortPressGesture)
        
        return content
            .gesture(combinedGesture)
            .onChange(of: eventState, perform: { (value) in
                print("onChange monitor value")
                print(value)
                switch value {
                case .pressing:
                    self.changeState(.long)
                case .dragging:
                    self.changeState(.dragOut)
                default:
                    return
                }
            })
            .onChange(of: isLongPress, perform: { (value) in
                print("onChange for tap")
                print(value)
            })
    }
}

// Extension to make the modifier available to any view
// Usage:
// Component.onTapLongPressDragEvent(changeState: { (buttonState) in {code} })
// E.g. Text("").foregroundColor(.white).onTapLongPressDragEvent(...)
public extension View {
    func onTapLongPressDragEvent(changeState: @escaping (EventAction) -> Void) -> some View {
        modifier(TapLongPressDragEventModifier(changeState: changeState))
    }
}
