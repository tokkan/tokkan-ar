//
//  TopBarHeader.swift
//  react-native-tokkan-ar
//
//  Created by glen rickard on 2021-01-20.
//

import SwiftUI

struct TopBarHeader: View {
    let onBackAction: () -> Void
    let centerView: AnyView?
    let rightView: AnyView?
//    @State private var middleView: AnyView?
//    @State private var rightView: AnyView?
//
//    init(
//        middleView: AnyView?,
//        rightView: AnyView?
//    ) {
//        self.middleView = middleView
//        self.rightView = rightView
//    }
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                BackArrowButton(
                    onBackAction: onBackAction
                )
                if centerView != nil {
                    centerView
                }
                if rightView != nil {
                    rightView
                }
            }
        }
        .padding(EdgeInsets(top: 15, leading: 5, bottom: 15, trailing: 0))
    }
}

//struct TopBarHeader_Previews: PreviewProvider {
//    static var previews: some View {
//        TopBarHeader()
//    }
//}

// MARK: - Back Arrow Button

struct BackArrowButton: View {
    let onBackAction: () -> Void
    
    var body: some View {
        Button(action: {
            print("Back action")
            onBackAction()
        }) {
            Image(systemName: "chevron.left")
                .padding()
                .aspectRatio(contentMode: .fit)
                .font(.largeTitle)
                .foregroundColor(.white)
        }
    }
}
