import SwiftUI

struct ARPreviewImageView: View {
    @EnvironmentObject var viewRouter: ViewRouter
    //    @ObservedObject var viewModel = ARPreviewImageViewModel()
    //    @Binding var selectedScreen: ARCaptureScreens
    //    @Binding var showPreview: Bool
    @Binding var arResult: ARCaptureResult?
    @State private var image: UIImage?
    let onNavigate: (_ screen: ARCaptureScreens) -> Void
    
    //    init() {
    ////        guard let url = arResult?.url else {
    //////            showPreview = false
    ////            return
    ////        }
    ////        image = ImageUtility.loadImageByUrl(url: url)
    //    }
    
    func backAction() {
        print("back action")
        if let url = arResult?.url {
            ImageUtility.deleteImage(url: url)
        }
        onNavigate(.capture)
    }
    
    var body: some View {
        ZStack {
            if let img = ImageUtility.loadImageByUrl(url: arResult!.url) {
                Image(uiImage: img)
                    .resizable()
//                    .rotationEffect(.degrees(90))
//                    .scaleEffect(2)
//                    .scaledToFit()
            }
            HStack {
                ARPreviewImageOverlayView(
                    onBackAction: backAction
                )
                Spacer()
            }
        }
    }
}
