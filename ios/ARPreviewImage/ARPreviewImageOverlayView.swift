import SwiftUI

struct ARPreviewImageOverlayView: View {
    let onBackAction: () -> Void
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                TopBarHeader(
                    onBackAction: onBackAction,
                    centerView: nil,
                    rightView: nil
                )
                Spacer()
            }
        }
    }
}

// MARK: - Overlay Header

struct ARPreviewImageOverlayHeader: View {
    let onBackAction: () -> Void
    
    var body: some View {
        EmptyView()
    }
}
