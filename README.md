# react-native-tokkan-ar

Tokkan AR

## Installation

```sh
npm install react-native-tokkan-ar
```

## Usage

```js
import TokkanAr from "react-native-tokkan-ar";

// ...

const result = await TokkanAr.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
