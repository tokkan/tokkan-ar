import * as React from 'react';

import { Button, Platform, StyleSheet, Text, View } from 'react-native';
// import TokkanArViewManager from 'react-native-tokkan-ar';
import TokkanAr, { ARCaptureView } from 'react-native-tokkan-ar'

export default function App() {
  const [result, setResult] = React.useState<number | undefined>();

  const onShow = async () => {
    // console.log("onShow in RN")
    const res = await TokkanAr.show()
    // console.log(res)
    // console.log("after onShow res")
    // const res = Connect.goToNative()
    console.log('After iOS goToNative')
    console.log(res)
  }

  const TokkanAR = () => {
    if (Platform.OS === 'ios') {
      return (
        <ARCaptureView
          // color="#32a852"
          style={styles.box}
        >
          {/* <Button
          title='RCT Button'
          onPress={() => console.log('Clicked RCT Button')}
        /> */}
        </ARCaptureView>
      )
    } else {
      return <>
        <Text>Result: {result}</Text>
        <Button
          title="SNAP"
          // onPress={() => TokkanAr.show()}
          onPress={() => onShow()}
        />
      </>
    }
  }

  return (
    <View style={styles.container}>
      {/* <TokkanArViewManager color="#32a852" style={styles.box} /> */}
      <TokkanAR />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    // display: 'flex',
    flex: 1,
    width: '100%',
    // width: 260,
    // height: 260,
    // marginVertical: 20,
    // backgroundColor: 'yellow',
  },
});
