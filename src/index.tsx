import { requireNativeComponent, ViewStyle } from 'react-native';
import { NativeModules } from 'react-native'

type TokkanArType = {
  show(): Promise<any>
}

const { TokkanAr } = NativeModules
export default TokkanAr as TokkanArType

type TokkanArProps = {
  color?: string;
  style?: ViewStyle;
};

export const TokkanArViewManager = requireNativeComponent<TokkanArProps>(
  'TokkanArView'
);

export const ARCaptureView = requireNativeComponent<TokkanArProps>('ARCaptureView')

// export default ARCaptureView

// export default TokkanArViewManager;
