//#version 120
//
//void main() {
//    gl_Position = vec4(vec3(0.0), 1.0);
//}

attribute vec4 aPosition;
attribute vec2 aTexPosition;
varying vec2 vTexPosition;

void main() {
  gl_Position = aPosition;
  vTexPosition = aTexPosition;
}
