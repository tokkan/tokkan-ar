//#extension GL_OES_EGL_image_external : require
//precision mediump float;
//varying vec2 vTexCoord;
//uniform samplerExternalOES sTexture;
//void main() {
//    //gl_FragColor=texture2D(sTexture, vTexCoord);
//    vec3 centralColor = texture2D(sTexture, vTexCoord).rgb;
//    gl_FragColor = vec4(0.299*centralColor.r+0.587*centralColor.g+0.114*centralColor.b);
//}

#extension GL_OES_EGL_image_external : require

precision mediump float;
varying vec2 vTexCoord;
uniform samplerExternalOES sTexture;

void main() {
  vec4 input_color = texture2D(sTexture, vTexCoord).rgba;
  gl_FragColor = input_color;
}
