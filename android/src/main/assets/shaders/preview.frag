//#version 120
//
//void main() {
//    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
//}

precision mediump float;
uniform sampler2D uTexture;
varying vec2 vTexPosition;

void main() {
  gl_FragColor = texture2D(uTexture, vTexPosition);
}
