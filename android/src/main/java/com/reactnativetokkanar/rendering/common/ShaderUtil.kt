package com.reactnativetokkanar.rendering.common

import android.content.Context
import android.opengl.GLES20
import android.util.Log
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*


/** Shader helper functions.  */
object ShaderUtil {
  private val TAG = ShaderUtil::class.simpleName

  /**
   * Converts a raw text file, saved as a resource, into an OpenGL ES shader.
   *
   * @param type The type of shader we will be creating.
   * @param filename The filename of the asset file about to be turned into a shader.
   * @param defineValuesMap The #define values to add to the top of the shader source code.
   * @return The shader object handler.
   */
  @Throws(IOException::class)
  fun loadGLShader(
    tag: String?, context: Context, type: Int, filename: String, defineValuesMap: Map<String, Int>): Int {
    // Load shader source code.
    var code = readShaderFileFromAssets(context, filename)

    // Prepend any #define values specified during this run.
    var defines = ""
    for ((key, value) in defineValuesMap) {
      defines += """#define $key $value
"""
    }
    code = defines + code

    // Compiles shader code.
    var shader = GLES20.glCreateShader(type)
    GLES20.glShaderSource(shader, code)
    GLES20.glCompileShader(shader)

    // Get the compilation status.
    val compileStatus = IntArray(1)
    GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compileStatus, 0)

    // If the compilation failed, delete the shader.
    if (compileStatus[0] == 0) {
      Log.e(tag, "Error compiling shader: " + GLES20.glGetShaderInfoLog(shader))
      GLES20.glDeleteShader(shader)
      shader = 0
    }
    if (shader == 0) {
      throw RuntimeException("Error creating shader.")
    }
    return shader
  }

  /** Overload of loadGLShader that assumes no additional #define values to add.  */
  @Throws(IOException::class)
  fun loadGLShader(tag: String?, context: Context, type: Int, filename: String): Int {
    val emptyDefineValuesMap: Map<String, Int> = TreeMap()
    return loadGLShader(tag, context, type, filename, emptyDefineValuesMap)
  }

  /**
   * Checks if we've had an error inside of OpenGL ES, and if so what that error is.
   *
   * @param label Label to report in case of error.
   * @throws RuntimeException If an OpenGL error is detected.
   */
  fun checkGLError(tag: String?, label: String) {
    var lastError = GLES20.GL_NO_ERROR
    // Drain the queue of all errors.
    var error: Int
    while (GLES20.glGetError().also { error = it } != GLES20.GL_NO_ERROR) {
      Log.e(tag, "$label: glError $error")
      lastError = error
    }
    if (lastError != GLES20.GL_NO_ERROR) {
      throw RuntimeException("$label: glError $lastError")
    }
  }

  /**
   * Converts a raw shader file into a string.
   *
   * @param filename The filename of the shader file about to be turned into a shader.
   * @return The context of the text file, or null in case of error.
   */
  @Throws(IOException::class)
  private fun readShaderFileFromAssets(context: Context, filename: String): String {
    Log.d(TAG, "readShaderFileFromAssets, filename: $filename")
    context.assets.open(filename).use { inputStream ->
      Log.d(TAG, "Opening inputStream")
      BufferedReader(InputStreamReader(inputStream)).use { reader ->
        Log.d(TAG, "Reading with bufferedReader")
        val sb = StringBuilder()
        var line: String
//        while (reader.readLine().also { line = it } != null) {
        reader.lineSequence().forEach {
          line = it
//          Log.d(TAG, "while reading reader")
//          Log.d(TAG, "line: $line")
//          Log.d(TAG, "line length: ${line.length}")
          val tokens = line.split(" ").dropLastWhile { it.isEmpty() }.toTypedArray()
//          Log.d(TAG, "tokens size: ${tokens.size}")
//          Log.d(TAG, tokens.joinToString { "," })

//          if (line.isEmpty()) {
//            Log.d(TAG, "Line is empty, continuing with next loop")
//            continue
//          }

          if (!tokens.isNullOrEmpty() && tokens[0] == "#include") {
            var includeFilename = tokens[1]
            includeFilename = includeFilename.replace("\"", "")
            if (includeFilename == filename) {
              throw IOException("Do not include the calling file.")
            }
            sb.append(readShaderFileFromAssets(context, includeFilename))
//            Log.d(TAG, "Appended to sb 1")
          } else if (!line.isNullOrEmpty()) {
            sb.append(line).append("\n")
//            Log.d(TAG, "Appended to sb 2")
          }
        }
        Log.d(TAG, "Returning SB")
//        Log.d(TAG, sb.toString())
        return sb.toString()
      }
    }
  }
}
