package com.reactnativetokkanar.rendering.preview

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.effect.EffectContext
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.GLUtils
import com.reactnativetokkanar.effects.ImageEffects
import com.reactnativetokkanar.effects.ImageEffectsTypes
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class ImageRenderer(filePath: String) : GLSurfaceView.Renderer {
  private var photo: Bitmap? = null
  private var photoWidth = 0
  private var photoHeight = 0
  private var textures = IntArray(2)
  private var square: Square? = null
  private var glWidth = 1
  private var glHeight = 1
  private var imageEffects: ImageEffects? = null
  private var requestEffect = false
  private val effectsList = listOf<String>()
  private var selectedEffect = ""

  init {
    loadImage(filePath)
  }

  fun loadImage(filePath: String) {
    // TODO: If rotation hasn't been fixed see ways to recognize rotation
    // https://developer.android.com/training/camerax/orientation-rotation
    photo = BitmapFactory.decodeFile(filePath)
    photoWidth = photo!!.width
    photoHeight = photo!!.height
  }

  private fun generateSquare() {
    GLES20.glGenTextures(2, textures, 0)
    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0])
    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR)
    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
    GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, photo, 0)
    square = Square()
  }

  override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {}

  override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
    glWidth = width
    glHeight = height
    GLES20.glViewport(0,0,width, height)
    GLES20.glClearColor(0f,0f,0f,1f)
    generateSquare()
  }

  override fun onDrawFrame(gl: GL10?) {
    if (imageEffects == null) {
      imageEffects = ImageEffects(EffectContext.createWithCurrentGlContext())
    }
    imageEffects?.releaseEffect()
    textures = imageEffects!!.applyEffect(ImageEffectsTypes.GRAYSCALE, textures, glWidth, glHeight)
//    square?.draw(textures[0])
    square?.draw(textures[1])
  }

  fun changeEffect() {
  }
}
