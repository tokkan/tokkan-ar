package com.reactnativetokkanar.rendering.preview

import android.content.Context
import android.graphics.SurfaceTexture
import android.graphics.SurfaceTexture.OnFrameAvailableListener
import android.media.MediaPlayer
import android.media.MediaPlayer.OnVideoSizeChangedListener
import android.media.effect.EffectContext
import android.opengl.*
import android.util.Log
import android.view.Surface
import com.reactnativetokkanar.effects.ImageEffects
import com.reactnativetokkanar.effects.ImageEffectsTypes
import com.reactnativetokkanar.rendering.common.ShaderUtil
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class GLVideoRenderer(private val context: Context, videoPath: String?) :
  GLSurfaceView.Renderer,
  OnFrameAvailableListener,
  OnVideoSizeChangedListener {
  private var aPositionLocation = 0
  private val vertexBuffer: FloatBuffer
  private var programId = 0
  private var vertexShader = 0
  private var fragmentShader = 0

  private val projectionMatrix = FloatArray(16)
  private var uMatrixLocation = 0

  private val textureVertexBuffer: FloatBuffer
  private var uTextureSamplerLocation = 0
  private var aTextureCoordLocation = 0
  private var textureId = 0
  private var surfaceTexture: SurfaceTexture? = null
  private var mediaPlayer: MediaPlayer? = null
  private val mSTMatrix = FloatArray(16)
  private var uSTMMatrixHandle = 0
  private var updateSurface = false
  private var screenWidth = 0
  private var screenHeight = 0
  private var imageEffects: ImageEffects? = null
  private var requestEffect = false
  private val effectsList = listOf<String>()
  private var selectedEffect = ""
  private var textures = IntArray(2)

  companion object {
    private val tagName = GLVideoRenderer::class.java.simpleName

    private val vertexData = floatArrayOf(
      1f, -1f, 0f,
      -1f, -1f, 0f,
      1f, 1f, 0f,
      -1f, 1f, 0f
    )
    private val textureVertexData = floatArrayOf(
//    1f, 0f,
//    0f, 0f,
//    1f, 1f,
//    0f, 1f
//    1f, 0f, 0f, 0f, 1f, 1f, 0f, 1f
//    0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f
//    0.0f, 0.0f,
//    0.0f, 1.0f,
//    1.0f, 0.0f,
//    1.0f, 1.0f
      0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f
    )
  }

  override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
    initializeProgram()

    aPositionLocation = GLES20.glGetAttribLocation(programId, "aPosition")
    uMatrixLocation = GLES20.glGetUniformLocation(programId, "uMatrix")
    uSTMMatrixHandle = GLES20.glGetUniformLocation(programId, "uSTMatrix")
    uTextureSamplerLocation = GLES20.glGetUniformLocation(programId, "sTexture")
    aTextureCoordLocation = GLES20.glGetAttribLocation(programId, "aTexCoord")
    GLES20.glGenTextures(2, textures, 0)
    textureId = textures[0]
    GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureId)
    ShaderUtil.checkGLError(tagName, "glBindTexture mTextureID")
    /*GLES11Ext.GL_TEXTURE_EXTERNAL_OES Its usefulness?
      As mentioned before, the output format of video decoding is YUV (YUV420p, should be), so the function of this extended texture is to realize the automatic conversion from YUV format to RGB,
      We don't need to write the code of YUV to RGB anymore*/
    GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER,
      GLES20.GL_NEAREST.toFloat())
    GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER,
      GLES20.GL_LINEAR.toFloat())

    surfaceTexture = SurfaceTexture(textureId)
    surfaceTexture!!.setOnFrameAvailableListener(this) //Monitor whether a new frame of data arrives
    val surface = Surface(surfaceTexture)
//    surface.lockHardwareCanvas().rotate(270f)
    mediaPlayer!!.setSurface(surface)
  }

  override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
    Log.d(tagName, "onSurfaceChanged: $width $height")
    screenWidth = width
    screenHeight = height
    GLES20.glViewport(0, 0, width, height)
    GLES20.glClearColor(0f, 0f, 0f, 1f)

//    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0])
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR)
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
//    GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, photo, 0)

    mediaPlayer!!.prepareAsync()
    mediaPlayer!!.setOnPreparedListener { mediaPlayer -> mediaPlayer.start() }
  }

  override fun onDrawFrame(gl: GL10) {
    GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT or GLES20.GL_COLOR_BUFFER_BIT)
    synchronized(this) {
      if (updateSurface) {
        surfaceTexture!!.updateTexImage() //Get new data
        surfaceTexture!!.getTransformMatrix(mSTMatrix) //The definition of mSTMatrix is exactly the same as that of projectionMatrix.
        updateSurface = false
      }
    }

//    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0])
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR)
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
//    GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
//
//    if (imageEffects == null) {
//      imageEffects = ImageEffects(EffectContext.createWithCurrentGlContext())
//    }
//    imageEffects?.releaseEffect()
//    textures = imageEffects!!.applyEffect(
//      ImageEffectsTypes.GRAYSCALE,
//      textures,
//      screenWidth,
//      screenHeight
//    )
////    textureId = textures[0]
//    textureId = textures[1]

    GLES20.glUseProgram(programId)
    GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, projectionMatrix, 0)
    GLES20.glUniformMatrix4fv(uSTMMatrixHandle, 1, false, mSTMatrix, 0)
    vertexBuffer.position(0)
    GLES20.glEnableVertexAttribArray(aPositionLocation)
    GLES20.glVertexAttribPointer(aPositionLocation, 3, GLES20.GL_FLOAT, false,
      12, vertexBuffer)
    textureVertexBuffer.position(0)
    GLES20.glEnableVertexAttribArray(aTextureCoordLocation)
    GLES20.glVertexAttribPointer(aTextureCoordLocation, 2, GLES20.GL_FLOAT, false, 8, textureVertexBuffer)
    GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
    GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureId)
    GLES20.glUniform1i(uTextureSamplerLocation, 0)
    GLES20.glViewport(0, 0, screenWidth, screenHeight)
    GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4)
  }

  @Synchronized
  override fun onFrameAvailable(surface: SurfaceTexture) {
    updateSurface = true
  }

  override fun onVideoSizeChanged(mp: MediaPlayer, width: Int, height: Int) {
    Log.d(tagName, "onVideoSizeChanged: $width $height")
//    updateProjection(width, height)
    updateProjection(height, width)
  }

  private fun initializeProgram() {
    //    val vertexShader: Int = ShaderUtil.loadGLShader(tagName, context, GLES20.GL_VERTEX_SHADER, "shaders/video.vert")
    vertexShader = ShaderUtil.loadGLShader(tagName, context, GLES20.GL_VERTEX_SHADER, "shaders/video.vert")
//    val fragmentShader: Int = ShaderUtil.loadGLShader(tagName, context, GLES20.GL_FRAGMENT_SHADER, "shaders/video.frag")
    fragmentShader = ShaderUtil.loadGLShader(tagName, context, GLES20.GL_FRAGMENT_SHADER, "shaders/video.frag")
    programId = GLES20.glCreateProgram()
    GLES20.glAttachShader(programId, vertexShader)
    GLES20.glAttachShader(programId, fragmentShader)
    GLES20.glLinkProgram(programId)
  }

  private fun initMediaPlayer() {
    mediaPlayer = MediaPlayer()
    try {
//      val afd = context.assets.openFd("big_buck_bunny.mp4")
//      val afd = context.assets.openFd("/data/data/com.example.reactnativetokkanar/cache/img1223523744705731713.mp4")
//      mediaPlayer!!.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
      mediaPlayer?.setDataSource("/data/data/com.example.reactnativetokkanar/cache/img1223523744705731713.mp4")
      //            String path = "http://192.168.1.254:8192";
//            mediaPlayer.setDataSource(path);
//            mediaPlayer.setDataSource(TextureViewMediaActivity.videoPath);
    } catch (e: IOException) {
      e.printStackTrace()
    }
//    mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
//    mediaPlayer!!.setAudioAttributes(AudioAttributes.USAGE_MEDIA)
    mediaPlayer!!.isLooping = true
    mediaPlayer!!.setOnVideoSizeChangedListener(this)
  }

  private fun updateProjection(videoWidth: Int, videoHeight: Int) {
    val screenRatio = screenWidth.toFloat() / screenHeight
    val videoRatio = videoWidth.toFloat() / videoHeight
    if (videoRatio > screenRatio) {
      Matrix.orthoM(projectionMatrix, 0, -1f, 1f, -videoRatio / screenRatio, videoRatio / screenRatio, -1f, 1f)
    } else Matrix.orthoM(projectionMatrix, 0, -screenRatio / videoRatio, screenRatio / videoRatio, -1f, 1f, -1f, 1f)
  }

  init {
    synchronized(this) { updateSurface = false }
    vertexBuffer = ByteBuffer.allocateDirect(vertexData.size * 4)
      .order(ByteOrder.nativeOrder())
      .asFloatBuffer()
      .put(vertexData)
    vertexBuffer.position(0)
    textureVertexBuffer = ByteBuffer.allocateDirect(textureVertexData.size * 4)
      .order(ByteOrder.nativeOrder())
      .asFloatBuffer()
      .put(textureVertexData)
    textureVertexBuffer.position(0)
    initMediaPlayer()
  }
}

