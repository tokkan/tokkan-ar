package com.reactnativetokkanar.rendering.preview

import android.content.Context
import android.graphics.SurfaceTexture
import android.graphics.SurfaceTexture.OnFrameAvailableListener
import android.media.MediaPlayer
import android.opengl.GLES11Ext
import android.opengl.GLES20
import android.opengl.Matrix
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Surface
import com.reactnativetokkanar.rendering.common.ShaderUtil.checkGLError
import java.io.IOException
import java.nio.Buffer
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.util.concurrent.locks.ReentrantLock
import javax.microedition.khronos.opengles.GL10

class VideoRenderer : OnFrameAvailableListener {
  private var videoTexture: SurfaceTexture? = null
  private var mTextureId = 0
  private val tagName = VideoRenderer::class.java.simpleName
  private var mQuadProgram = 0
//  private val lock = Any()
  private val lock = ReentrantLock()
  private val modelViewProjection = FloatArray(16)
  private val modelView = FloatArray(16)
  private var mQuadVertices: FloatBuffer? = null
  private val mModelMatrix = FloatArray(16)
  private var player: MediaPlayer? = null
  private var frameAvailable = false
  private var done = false
  private var prepared = false
  var isStarted = false
//    private set
  private var mTexCoordTransformationMatrix: Array<FloatArray> = emptyArray()
  private var mQuadPositionParam = 0
  private var mQuadTexCoordParam = 0
  private var mModelViewProjectionUniform = 0
  private val VIDEO_QUAD_TEXTCOORDS_TRANSFORMED = floatArrayOf(0.0f, 0.0f,
    1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f)

  fun createOnGlThread() {
    // 1 texture to hold the video frame.
    val textures = IntArray(1)
    GLES20.glGenTextures(1, textures, 0)
    mTextureId = textures[0]
    val mTextureTarget = GLES11Ext.GL_TEXTURE_EXTERNAL_OES
    GLES20.glBindTexture(mTextureTarget, mTextureId)
    GLES20.glTexParameteri(mTextureTarget, GLES20.GL_TEXTURE_MIN_FILTER,
      GLES20.GL_NEAREST)
    GLES20.glTexParameteri(mTextureTarget, GLES20.GL_TEXTURE_MAG_FILTER,
      GLES20.GL_NEAREST)
    videoTexture = SurfaceTexture(mTextureId)
    videoTexture!!.setOnFrameAvailableListener(this)
    mTexCoordTransformationMatrix = Array(1) { FloatArray(16) }
    createQuardCoord()
    createQuadTextCoord()
    val vertexShader: Int = Companion.loadGLShader(GLES20.GL_VERTEX_SHADER, VERTEX_SHADER)
    val fragmentShader: Int = Companion.loadGLShader(
      GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER)
    mQuadProgram = GLES20.glCreateProgram()
    GLES20.glAttachShader(mQuadProgram, vertexShader)
    GLES20.glAttachShader(mQuadProgram, fragmentShader)
    GLES20.glLinkProgram(mQuadProgram)
    GLES20.glUseProgram(mQuadProgram)
    checkGLError(tagName, "Program creation")
    mQuadPositionParam = GLES20.glGetAttribLocation(mQuadProgram, "a_Position")
    mQuadTexCoordParam = GLES20.glGetAttribLocation(mQuadProgram, "a_TexCoord")
    mModelViewProjectionUniform = GLES20.glGetUniformLocation(
      mQuadProgram, "u_ModelViewProjection")
    checkGLError(tagName, "Program parameters")
    Matrix.setIdentityM(mModelMatrix, 0)
    initializeMediaPlayer()
  }

  fun update(modelMatrix: FloatArray?) {
    val scaleMatrix = FloatArray(16)
    Matrix.setIdentityM(scaleMatrix, 0)
    val SCALE_FACTOR = 1f
    scaleMatrix[0] = SCALE_FACTOR
    scaleMatrix[5] = SCALE_FACTOR
    scaleMatrix[10] = SCALE_FACTOR
    Matrix.multiplyMM(mModelMatrix, 0, modelMatrix, 0, scaleMatrix, 0)
  }

  fun draw(cameraView: FloatArray?, cameraPerspective: FloatArray?) {
    if (done || !prepared) {
      return
    }
    synchronized(this) {
      if (frameAvailable) {
        videoTexture!!.updateTexImage()
        frameAvailable = false
        if (videoTexture != null) {
          videoTexture!!.getTransformMatrix(mTexCoordTransformationMatrix[0])
          setVideoDimensions(mTexCoordTransformationMatrix[0])
          createQuadTextCoord()
        }
      }
    }
    Matrix.multiplyMM(modelView, 0, cameraView, 0, mModelMatrix, 0)
    Matrix.multiplyMM(modelViewProjection, 0, cameraPerspective, 0, modelView, 0)

    // ShaderUtil.checkGLError(tagName, "Before draw");
    GLES20.glEnable(GL10.GL_BLEND)
    GLES20.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA)
    GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureId)
    GLES20.glUseProgram(mQuadProgram)


    // Set the vertex positions.
    GLES20.glVertexAttribPointer(mQuadPositionParam, Companion.COORDS_PER_VERTEX,
      GLES20.GL_FLOAT, false, 0, mQuadVertices)

    // Set the texture coordinates.
    GLES20.glVertexAttribPointer(mQuadTexCoordParam, Companion.TEXCOORDS_PER_VERTEX,
      GLES20.GL_FLOAT, false, 0, fillBuffer(VIDEO_QUAD_TEXTCOORDS_TRANSFORMED))

    // Enable vertex arrays
    GLES20.glEnableVertexAttribArray(mQuadPositionParam)
    GLES20.glEnableVertexAttribArray(mQuadTexCoordParam)
    GLES20.glUniformMatrix4fv(mModelViewProjectionUniform, 1, false,
      modelViewProjection, 0)
    GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4)

    // Disable vertex arrays
    GLES20.glDisableVertexAttribArray(mQuadPositionParam)
    GLES20.glDisableVertexAttribArray(mQuadTexCoordParam)
    checkGLError(tagName, "Draw")
  }

  private fun setVideoDimensions(textureCoordMatrix: FloatArray) {
    var tempUVMultRes: FloatArray
    tempUVMultRes = uvMultMat4f(Companion.QUAD_TEXCOORDS.get(0), Companion.QUAD_TEXCOORDS.get(1), textureCoordMatrix)
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[0] = tempUVMultRes[0]
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[1] = tempUVMultRes[1]
    tempUVMultRes = uvMultMat4f(Companion.QUAD_TEXCOORDS.get(2), Companion.QUAD_TEXCOORDS.get(3), textureCoordMatrix)
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[2] = tempUVMultRes[0]
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[3] = tempUVMultRes[1]
    tempUVMultRes = uvMultMat4f(Companion.QUAD_TEXCOORDS.get(4), Companion.QUAD_TEXCOORDS.get(5), textureCoordMatrix)
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[4] = tempUVMultRes[0]
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[5] = tempUVMultRes[1]
    tempUVMultRes = uvMultMat4f(Companion.QUAD_TEXCOORDS.get(6), Companion.QUAD_TEXCOORDS.get(7), textureCoordMatrix)
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[6] = tempUVMultRes[0]
    VIDEO_QUAD_TEXTCOORDS_TRANSFORMED[7] = tempUVMultRes[1]
  }

  fun play(filename: String?, context: Context): Boolean {
    Log.d(tagName, "Play video")
    if (player == null) {
//      synchronized(lock) {
//        while (player == null) {
//          try {
//            lock.wait()
//          } catch (e: InterruptedException) {
//            return false
//          }
//        }
//      }
      Log.d(tagName, "Video not available, check lock in kotlin")

      return false
    }
    Log.d(tagName, "Actually playing video :3")
    player!!.reset()
    done = false
    player!!.setOnPreparedListener { mp: MediaPlayer ->
      prepared = true
      mp.start()
    }
    player!!.setOnErrorListener { mp: MediaPlayer?, what: Int, extra: Int ->
      done = true
      false
    }
    player!!.setOnCompletionListener { mp: MediaPlayer? -> done = true }
    player!!.setOnInfoListener { mediaPlayer: MediaPlayer?, i: Int, i1: Int -> false }
    try {
      val assets = context.assets
      val descriptor = assets.openFd(filename!!)
      player!!.setDataSource(descriptor.fileDescriptor,
        descriptor.startOffset,
        descriptor.length)
      player!!.setSurface(Surface(videoTexture))
      player!!.isLooping = true
      player!!.prepareAsync()
      synchronized(this) { isStarted = true }
    } catch (e: IOException) {
      Log.e(tagName, "Exception preparing movie", e)
      return false
    }
    return true
  }

  private fun initializeMediaPlayer() {
    Log.d(tagName, "Initializing Media Player")
    if (handler == null) {
      handler = Handler(Looper.getMainLooper())
    }
    handler!!.post {
//      synchronized(lock) {
        player = MediaPlayer()
////        lock.notify()
//      lock.unlock()
//      }
    }
  }

  override fun onFrameAvailable(surfaceTexture: SurfaceTexture) {
    synchronized(this) { frameAvailable = true }
  }

  private fun createQuadTextCoord() {
    val numVertices = 4
    val bbTexCoords = ByteBuffer.allocateDirect(
      numVertices * Companion.TEXCOORDS_PER_VERTEX * Companion.FLOAT_SIZE)
    bbTexCoords.order(ByteOrder.nativeOrder())
    val mQuadTexCoord = bbTexCoords.asFloatBuffer()
    mQuadTexCoord.put(Companion.QUAD_TEXCOORDS)
    mQuadTexCoord.position(0)
  }

  // Make a quad to hold the movie
  private fun createQuardCoord() {
    val bbVertices = ByteBuffer.allocateDirect(
      Companion.QUAD_COORDS.size * Companion.FLOAT_SIZE)
    bbVertices.order(ByteOrder.nativeOrder())
    mQuadVertices = bbVertices.asFloatBuffer()
    mQuadVertices?.put(Companion.QUAD_COORDS)
    mQuadVertices?.position(0)
  }

  private fun fillBuffer(array: FloatArray): Buffer {
    // Convert to floats because OpenGL doesnt work on doubles, and manually
    // casting each input value would take too much time.
    val bb = ByteBuffer.allocateDirect(4 * array.size) // each float takes 4 bytes
    bb.order(ByteOrder.LITTLE_ENDIAN)
    for (d in array) bb.putFloat(d)
    bb.rewind()
    return bb
  }

  private fun uvMultMat4f(u: Float,
                          v: Float, pMat: FloatArray): FloatArray {
    val x = pMat[0] * u + pMat[4] * v + (pMat[12]
      * 1f)
    val y = pMat[1] * u + pMat[5] * v + (pMat[13]
      * 1f)
    val result = FloatArray(2)
    result[0] = x
    result[1] = y
    return result
  }

  companion object {
    private const val TEXCOORDS_PER_VERTEX = 2
    private const val COORDS_PER_VERTEX = 3
    private const val VERTEX_SHADER = "uniform mat4 u_ModelViewProjection;\n\n" +
      "attribute vec4 a_Position;\n" +
      "attribute vec2 a_TexCoord;\n\n" +
      "varying vec2 v_TexCoord;\n\n" +
      "void main() {\n" +
      "   gl_Position = u_ModelViewProjection * vec4(a_Position.xyz, 1.0);\n" +
      "   v_TexCoord = a_TexCoord;\n" +
      "}"
    private const val FRAGMENT_SHADER = "#extension GL_OES_EGL_image_external : require\n" +
      "\n" +
      "precision mediump float;\n" +
      "varying vec2 v_TexCoord;\n" +
      "uniform samplerExternalOES sTexture;\n" +
      "\n" +
      "void main() {\n" +
      "  vec4 input_color = texture2D(sTexture, v_TexCoord).rgba;\n" +
      "  gl_FragColor = input_color;\n" +
      "}"
    private val QUAD_COORDS = floatArrayOf(
      -1.0f, -1.0f, 0.0f,
      -1.0f, +1.0f, 0.0f,
      +1.0f, -1.0f, 0.0f,
      +1.0f, +1.0f, 0.0f)
    private val QUAD_TEXCOORDS = floatArrayOf(
      0.0f, 0.0f,
      0.0f, 1.0f,
      1.0f, 0.0f,
      1.0f, 1.0f
    )
    private const val FLOAT_SIZE = 4
    private var handler: Handler? = null

    private fun loadGLShader(glVertexShader: Int, vertexShader: String): Int {
      var shader = GLES20.glCreateShader(glVertexShader)
      GLES20.glShaderSource(shader, vertexShader)
      GLES20.glCompileShader(shader)
      val status = IntArray(1)
      GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, status, 0)
      if (status[0] == 0) {
        Log.e("SHADER", "Error in compiling shader: " + GLES20.glGetShaderInfoLog(shader))
        GLES20.glDeleteShader(shader)
        shader = 0
      }
      if (shader == 0) {
        throw RuntimeException("Error creating shader.")
      }
      return shader
    }
  }
}
