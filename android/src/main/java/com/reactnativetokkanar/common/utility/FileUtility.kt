package com.reactnativetokkanar.common.utility

import android.graphics.*
import android.media.Image
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

enum class FileExtensions(val value: String) {
  JPG(".jpg"),
  MP4(".mp4"),
}

// ImageFormats: https://developer.android.com/reference/android/graphics/ImageFormat#YUV_420_888
object FileUtility {
  private val tagName = FileUtility::class.java.simpleName
  private val cacheDirectory = Environment.getDownloadCacheDirectory()

  private fun getImageInfo(image: Image) {
    Log.d(tagName, "w: ${image.width}, h: ${image.height}")
  }

  fun fromImageToFile(image: Image): File? {
    getImageInfo(image)
    val buffer = fromImageToBuffer(image)
    if (buffer != null) {
      val yuvBuffer = fromNV21toJPEG(buffer, image.width, image.height)
//      return fromBufferToFile(buffer)
      if (yuvBuffer != null)
        return fromBufferToFile(yuvBuffer)
    }
    return null
  }

  private fun fromImageToBuffer(image: Image): ByteArray? {
    Log.d(tagName, "image format: ${image.format}")
    return when (image.format) {
      ImageFormat.YUV_420_888,
      ImageFormat.YUV_422_888,
      ImageFormat.YUV_444_888,
      -> {
        fromYUVToBuffer(image)
      }
      else -> {
        null
      }
    }
  }

  private fun fromBufferToFile(buffer: ByteArray): File {
//    val options = BitmapFactory.Options()
    val bitmap = BitmapFactory.decodeByteArray(buffer, 0, buffer.size)
    val fileId = UUID.randomUUID().toString()

    return try {
//      val file = File("${cacheDirectory.absolutePath}${File.separator}${fileId}.jpg")
      val fileDescriptor: ParcelFileDescriptor
      val filePath = "${cacheDirectory.absolutePath}${File.separator}${fileId}.jpg"
//      fileDescriptor =
      val file = File.createTempFile("img", ".jpg")
      val exists = file.createNewFile()
      Log.d(tagName, "Is file null or not: ${file == null}")
      Log.d(tagName, "Did file exist: $exists")
      Log.d(tagName, file.absolutePath)
      val fileStream = FileOutputStream(file)
      bitmap.compress(Bitmap.CompressFormat.JPEG, 64, fileStream)
      fileStream.close()
      file
    } catch (e: IOException) {
      Log.d(tagName, "IO Exception with writing to file")
      throw e
    }
  }

  fun removeFile(path: String): Boolean {
    val file = File(path)
    return file.delete()
  }

  private fun fromYUVToBuffer(image: Image): ByteArray {
    val buffer: ByteArray
    val yBuffer = image.planes[0].buffer
    val uBuffer = image.planes[1].buffer
    val vBuffer = image.planes[2].buffer
    val ySize = yBuffer.remaining()
    val uSize = uBuffer.remaining()
    val vSize = vBuffer.remaining()
    buffer = ByteArray(ySize + uSize + vSize)

    //U and V are swapped
    yBuffer[buffer, 0, ySize]
    vBuffer[buffer, ySize, vSize]
    uBuffer[buffer, ySize + vSize, uSize]
    return buffer
  }

  private fun fromNV21toJPEG(buffer: ByteArray, width: Int, height: Int, quality: Int = 64): ByteArray? {
    val out = ByteArrayOutputStream()
    val yuv = YuvImage(buffer, ImageFormat.NV21, width, height, null)
    yuv.compressToJpeg(Rect(0, 0, width, height), quality, out)
    return out.toByteArray()
  }

  fun fromBitmapToFile(bitmap: Bitmap): File {
    val fileId = UUID.randomUUID().toString()

    return try {
      val file = createTempFile(FileExtensions.JPG)
      val fileStream = FileOutputStream(file)
      bitmap.compress(Bitmap.CompressFormat.JPEG, 64, fileStream)
      fileStream.close()
      file
    } catch (e: IOException) {
      Log.d(tagName, "IO Exception with writing to file")
      throw e
    }
  }

  fun createTempFile(fileExtension: FileExtensions): File {
    return try {
      val file = File.createTempFile("tokkan-", fileExtension.value)
      val exists = file.createNewFile()
      Log.d(tagName, "Is file null or not: ${file == null}")
      Log.d(tagName, "Did file exist: $exists")
      Log.d(tagName, file.absolutePath)
      file
    } catch (e: IOException) {
      Log.d(tagName, "IO Exception with creating temp file")
      throw e
    }
  }
}
