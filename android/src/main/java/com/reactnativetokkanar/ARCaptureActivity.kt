package com.reactnativetokkanar

import android.content.Intent
import android.media.effect.EffectContext
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.ar.core.*
import com.google.ar.core.ArCoreApk.InstallStatus
import com.google.ar.core.AugmentedFace.RegionType
import com.google.ar.core.Config.AugmentedFaceMode
import com.google.ar.core.Session.Feature
import com.google.ar.core.exceptions.*
import com.reactnativetokkanar.common.helpers.CameraPermissionHelper.hasCameraPermission
import com.reactnativetokkanar.common.helpers.CameraPermissionHelper.launchPermissionSettings
import com.reactnativetokkanar.common.helpers.CameraPermissionHelper.requestCameraPermission
import com.reactnativetokkanar.common.helpers.CameraPermissionHelper.shouldShowRequestPermissionRationale
import com.reactnativetokkanar.common.helpers.DisplayRotationHelper
import com.reactnativetokkanar.common.helpers.FullScreenHelper.setFullScreenOnWindowFocusChanged
import com.reactnativetokkanar.common.helpers.SnackbarHelper
import com.reactnativetokkanar.common.helpers.TrackingStateHelper
import com.reactnativetokkanar.common.utility.FileExtensions
import com.reactnativetokkanar.common.utility.FileUtility
import com.reactnativetokkanar.effects.ImageEffects
import com.reactnativetokkanar.effects.ImageEffectsTypes
import com.reactnativetokkanar.rendering.common.ARFaceRenderer
import com.reactnativetokkanar.rendering.common.BackgroundRenderer
import com.reactnativetokkanar.rendering.common.GLRecording
import com.reactnativetokkanar.rendering.common.ObjectRenderer
import com.reactnativetokkanar.ui.main.ARCaptureFragment
import java.io.IOException
import java.util.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

// https://developer.android.com/training/camerax/orientation-rotation
class ARCaptureActivity : AppCompatActivity(), GLSurfaceView.Renderer {
  private val tagName: String = ARCaptureActivity::class.java.simpleName

  // Rendering. The Renderers are created here, and initialized when the GL surface is created.
  private lateinit var surfaceView: GLSurfaceView

  private var installRequested = false

  private var session: Session? = null
  private val messageSnackbarHelper = SnackbarHelper()
  private lateinit var displayRotationHelper: DisplayRotationHelper
  private val trackingStateHelper = TrackingStateHelper(this)

  private val backgroundRenderer = BackgroundRenderer()
  private val augmentedFaceRenderer: ARFaceRenderer = ARFaceRenderer()
  private val noseObject = ObjectRenderer()
  private val rightEarObject = ObjectRenderer()
  private val leftEarObject = ObjectRenderer()

  // Temporary matrix allocated here to reduce number of allocations for each frame.
  private val noseMatrix = FloatArray(16)
  private val rightEarMatrix = FloatArray(16)
  private val leftEarMatrix = FloatArray(16)
  private val defaultColorMatrix = floatArrayOf(0f, 0f, 0f, 0f)

  private var requestSnap = false
  private var requestRecording = false
  private var isRecording = false
  private var glWidth = 1
  private var glHeight = 1
  private var imageEffects: ImageEffects? = null

  private fun configureSession() {
    val config = Config(session)
    config.augmentedFaceMode = AugmentedFaceMode.MESH3D
    session!!.configure(config)
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    Log.d(tagName, "onActivityResult")
    super.onActivityResult(requestCode, resultCode, data)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.ar_capture_activity)

    surfaceView = findViewById(R.id.surfaceview)
    displayRotationHelper = DisplayRotationHelper(/*context=*/ this)

    // Set up renderer.
    surfaceView.preserveEGLContextOnPause = true
    surfaceView.setEGLContextClientVersion(2)
    surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0) // Alpha used for plane blending.
    surfaceView.setRenderer(this)
    surfaceView.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY
    surfaceView.setWillNotDraw(false)

    installRequested = false

    if (savedInstanceState == null) {
      supportFragmentManager.beginTransaction()
        .replace(R.id.container, ARCaptureFragment.newInstance())
        .commitNow()

////      this@ARCaptureActivity.replaceFragment(
////        R.id.shutter_button,
////        supportFragmentManager.findFragmentByTag(ARCaptureFragment::class.java.name)
////          ?: ARCaptureFragment.newInstance()
////      )
//      // Same as above without extension
//      supportFragmentManager.beginTransaction()
//        .replace(R.id.shutter_button,
//        supportFragmentManager.findFragmentByTag(ARCaptureFragment::class.java.name)
//        ?: ARCaptureFragment.newInstance())
//        .commit()
    }
  }

  override fun onDestroy() {
    if (session != null) {
      // Explicitly close ARCore Session to release native resources.
      // Review the API reference for important considerations before calling close() in apps with
      // more complicated lifecycle requirements:
      // https://developers.google.com/ar/reference/java/arcore/reference/com/google/ar/core/Session#close()
      session!!.close()
      session = null
    }
    super.onDestroy()
  }

  override fun onResume() {
    super.onResume()
    if (session == null) {
      var exception: Exception? = null
      var message: String? = null
      try {
        when (ArCoreApk.getInstance().requestInstall(this, !installRequested)) {
          InstallStatus.INSTALL_REQUESTED -> {
            installRequested = true
            return
          }
          InstallStatus.INSTALLED -> {
          }
          else -> {
          }
        }

        // ARCore requires camera permissions to operate. If we did not yet obtain runtime
        // permission on Android M and above, now is a good time to ask the user for it.
        if (!hasCameraPermission(this)) {
          requestCameraPermission(this)
          return
        }

        // Configure session to use front facing camera.
        val featureSet = EnumSet.of(Feature.FRONT_CAMERA)
        // Create the session.
        session = Session( /* context= */this, featureSet)
        configureSession()
      } catch (e: UnavailableArcoreNotInstalledException) {
        message = "Please install ARCore"
        exception = e
      } catch (e: UnavailableUserDeclinedInstallationException) {
        message = "Please install ARCore"
        exception = e
      } catch (e: UnavailableApkTooOldException) {
        message = "Please update ARCore"
        exception = e
      } catch (e: UnavailableSdkTooOldException) {
        message = "Please update this app"
        exception = e
      } catch (e: UnavailableDeviceNotCompatibleException) {
        message = "This device does not support AR"
        exception = e
      } catch (e: Exception) {
        message = "Failed to create AR session"
        exception = e
      }
      if (message != null) {
        messageSnackbarHelper.showError(this, message)
        Log.e(tagName, "Exception creating session", exception)
        return
      }
    }

    // Note that order matters - see the note in onPause(), the reverse applies here.
    try {
      session!!.resume()
    } catch (e: CameraNotAvailableException) {
      messageSnackbarHelper.showError(this, "Camera not available. Try restarting the app.")
      session = null
      return
    }
    surfaceView.onResume()
    displayRotationHelper.onResume()
  }

  override fun onPause() {
    super.onPause()
    if (session != null) {
      // Note that the order matters - GLSurfaceView is paused first so that it does not try
      // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
      // still call session.update() and get a SessionPausedException.
      displayRotationHelper.onPause()
      surfaceView.onPause()
      session!!.pause()
    }
  }

  //  fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>?, results: IntArray?) {
  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, results: IntArray) {
//    super.onRequestPermissionsResult(requestCode, permissions!!, results!!)
    super.onRequestPermissionsResult(requestCode, permissions, results)
    if (!hasCameraPermission(this)) {
      Toast.makeText(this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
        .show()
      if (!shouldShowRequestPermissionRationale(this)) {
        // Permission denied with checking "Do not ask again".
        launchPermissionSettings(this)
      }
      finish()
    }
  }

  override fun onWindowFocusChanged(hasFocus: Boolean) {
    super.onWindowFocusChanged(hasFocus)
    setFullScreenOnWindowFocusChanged(this, hasFocus)
  }

  override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
    GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f)

    // Prepare the rendering objects. This involves reading shaders, so may throw an IOException.
    try {
      // Create the texture and pass it to ARCore session to be filled during update().
      backgroundRenderer.createOnGlThread( /*context=*/this)
      augmentedFaceRenderer.createOnGlThread(this, "models/freckles.png")
      augmentedFaceRenderer.setMaterialProperties(0.0f, 1.0f, 0.1f, 6.0f)
      noseObject.createOnGlThread( /*context=*/
        this,
        "models/nose.obj",
        "models/nose_fur.png")
      noseObject.setMaterialProperties(0.0f, 1.0f, 0.1f, 6.0f)
      noseObject.setBlendMode(ObjectRenderer.BlendMode.AlphaBlending)
      rightEarObject.createOnGlThread(
        this, "models/forehead_right.obj", "models/ear_fur.png")
      rightEarObject.setMaterialProperties(0.0f, 1.0f, 0.1f, 6.0f)
      rightEarObject.setBlendMode(ObjectRenderer.BlendMode.AlphaBlending)
      leftEarObject.createOnGlThread(
        this, "models/forehead_left.obj", "models/ear_fur.png")
      leftEarObject.setMaterialProperties(0.0f, 1.0f, 0.1f, 6.0f)
      leftEarObject.setBlendMode(ObjectRenderer.BlendMode.AlphaBlending)
    } catch (e: IOException) {
      Log.e(tagName, "Failed to read an asset file", e)
    }
  }

  override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
    glWidth = width
    glHeight = height
    displayRotationHelper.onSurfaceChanged(width, height)
    GLES20.glViewport(0, 0, width, height)
  }

  override fun onDrawFrame(gl: GL10?) {
    // Clear screen to notify driver it should not load any pixels from previous frame.
    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
//    if (imageEffects == null) {
//      imageEffects = ImageEffects(EffectContext.createWithCurrentGlContext())
//    }
//    imageEffects?.releaseEffect()
//    imageEffects.applyEffect(ImageEffectsTypes.GRAYSCALE, )

    drawARFrame(gl)
  }

  private fun drawARFrame(gl: GL10?) {
    if (session == null) {
      return
    }
    // Notify ARCore session that the view size changed so that the perspective matrix and
    // the video background can be properly adjusted.
    displayRotationHelper.updateSessionIfNeeded(session!!)
    try {
      session!!.setCameraTextureName(backgroundRenderer.textureId)

      // Obtain the current frame from ARSession. When the configuration is set to
      // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
      // camera frame rate.
      val frame = session!!.update()
      val camera: Camera = frame.camera

      // Get projection matrix.
      val projectionMatrix = FloatArray(16)
      camera.getProjectionMatrix(projectionMatrix, 0, 0.1f, 100.0f)

      // Get camera matrix and draw.
      val viewMatrix = FloatArray(16)
      camera.getViewMatrix(viewMatrix, 0)

      // Compute lighting from average intensity of the image.
      // The first three components are color scaling factors.
      // The last one is the average pixel intensity in gamma space.
      val colorCorrectionRgba = FloatArray(4)
      frame.lightEstimate.getColorCorrection(colorCorrectionRgba, 0)

      // If frame is ready, render camera preview image to the GL surface.
      backgroundRenderer.draw(frame)

      // Keep the screen unlocked while tracking, but allow it to lock when tracking stops.
      trackingStateHelper.updateKeepScreenOnFlag(camera.trackingState)

      // ARCore's face detection works best on upright faces, relative to gravity.
      // If the device cannot determine a screen side aligned with gravity, face
      // detection may not work optimally.
      val faces = session!!.getAllTrackables(AugmentedFace::class.java)
      for (face in faces) {
        if (face.trackingState != TrackingState.TRACKING) {
          break
        }
        val scaleFactor = 1.0f

        // Face objects use transparency so they must be rendered back to front without depth write.
        GLES20.glDepthMask(false)

        // Each face's region poses, mesh vertices, and mesh normals are updated every frame.

        // 1. Render the face mesh first, behind any 3D objects attached to the face regions.
        val modelMatrix = FloatArray(16)
        face.centerPose.toMatrix(modelMatrix, 0)
        augmentedFaceRenderer.draw(
          projectionMatrix, viewMatrix, modelMatrix, colorCorrectionRgba, face)

        // 2. Next, render the 3D objects attached to the forehead.
        face.getRegionPose(RegionType.FOREHEAD_RIGHT).toMatrix(rightEarMatrix, 0)
        rightEarObject.updateModelMatrix(rightEarMatrix, scaleFactor)
        rightEarObject.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, defaultColorMatrix)
        face.getRegionPose(RegionType.FOREHEAD_LEFT).toMatrix(leftEarMatrix, 0)
        leftEarObject.updateModelMatrix(leftEarMatrix, scaleFactor)
        leftEarObject.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, defaultColorMatrix)

        // 3. Render the nose last so that it is not occluded by face mesh or by 3D objects attached
        // to the forehead regions.
        face.getRegionPose(RegionType.NOSE_TIP).toMatrix(noseMatrix, 0)
        noseObject.updateModelMatrix(noseMatrix, scaleFactor)
        noseObject.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, defaultColorMatrix)
      }

      if (requestSnap && !isRecording) {
        requestSnap = false
        if (gl != null) {
          handleGLImage(0, 0, glWidth, glHeight, gl)
        }
      }

      if (requestRecording && !isRecording) {
        requestRecording = false
        handleRecord()
      }
    } catch (t: Throwable) {
      // Avoid crashing the application due to unhandled exceptions.
      Log.e(tagName, "Exception on the OpenGL thread", t)
    } finally {
      GLES20.glDepthMask(true)
    }
  }

  private fun handleCameraImage() {
    Log.d(tagName, "saveImage")
    if (session != null) {
      val frame = session?.update()
      val image = frame?.acquireCameraImage()

      if (image != null) {
        val intent = Intent()
//        val buffer = FileUtility.toBuffer(image)
        val file = FileUtility.fromImageToFile(image)
        image.close()

        val filePath = file?.absolutePath ?: ""
        intent.putExtra("com.reactnativetokkanar.filepath", filePath)

        val previewIntent = Intent(this, ARPreviewActivity::class.java)
        previewIntent.putExtra("com.reactnativetokkanar.filepath", filePath)
//        this.setResult(RESULT_OK, intent)
        this.startActivityForResult(previewIntent, 2)
      } else {
//        this.setResult(-1)
      }

//      finish()
    }
  }

  private fun handleGLImage(x: Int, y: Int, w: Int, h: Int, gl: GL10) {
    val bitmap = GLRecording.createBitmapFromGLSurface(x, y, w, h, gl)

    if (bitmap != null) {
      val file = FileUtility.fromBitmapToFile(bitmap)
      val filePath = file?.absolutePath ?: ""
      intent.putExtra("com.reactnativetokkanar.filepath", filePath)

      val previewIntent = Intent(this, ARPreviewActivity::class.java)
      previewIntent.putExtra("com.reactnativetokkanar.filepath", filePath)
//        this.setResult(RESULT_OK, intent)
      this.startActivityForResult(previewIntent, 2)
    } else {
      Log.d(tagName, "Failed to create bitmap from GLSurface")
    }
  }

  private fun handleRecord() {
    if (session != null) {
      var recordingConfig = RecordingConfig(session!!)
//        recordingConfig.recordingRotation(90)
      // TODO: Create temp file
      // TODO: Have a counter, so amount of time recording is max e.g. 10 seconds
      // TODO: See if recording can be played in another activity
      val file = FileUtility.createTempFile(FileExtensions.MP4)
      val filePath = file.absolutePath ?: ""

      if (filePath.isNotEmpty()) {
        recordingConfig.mp4DatasetFilePath = filePath
        session?.startRecording(recordingConfig)
        isRecording = true
        Log.d(tagName, "Started recording")
      }
    }
  }

  fun onBack() {
    val intent = Intent()
    val empty: String? = null
    intent.putExtra("com.reactnativetokkanar.filepath", empty)
    this.setResult(RESULT_OK, intent)
    onBackPressed()
  }

  fun onSnap() {
    Log.d(tagName, "onSnap function")
    requestSnap = true
  }

  fun onRecord() {
    Log.d(tagName, "onRecord function")
    requestRecording = true
  }

  fun onStopRecord() {
    Log.d(tagName, "Requesting to stop recording")
    requestRecording = false
    if (session != null) {
      session?.stopRecording()
      isRecording = false
      Log.d(tagName, "Stopped recording")
    }
  }
}
