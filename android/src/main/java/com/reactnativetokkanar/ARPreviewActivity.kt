package com.reactnativetokkanar

import android.content.Intent
import android.opengl.GLSurfaceView
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.reactnativetokkanar.common.helpers.DisplayRotationHelper
import com.reactnativetokkanar.common.helpers.FullScreenHelper
import com.reactnativetokkanar.rendering.preview.ImageRenderer
import com.reactnativetokkanar.ui.main.ARPreviewFragment

class ARPreviewActivity : AppCompatActivity() {
  private val tagName = ARPreviewActivity::class.java.simpleName
  private lateinit var surfaceView: GLSurfaceView
  private var filePath: String = ""

  //  private val imageRenderer = ImageRenderer("")
  private lateinit var imageRenderer: ImageRenderer
  private lateinit var displayRotationHelper: DisplayRotationHelper

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    Log.d(tagName, "onActivityResult")
    super.onActivityResult(requestCode, resultCode, data)

    if (data?.hasExtra("com.reactnativetokkanar.filepath") == true) {
      filePath = intent.extras?.getString("com.reactnativetokkanar.filepath") ?: ""
      Log.d(tagName, "Has putExtra")
      Log.d(tagName, filePath)
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    Log.d(tagName, "onCreate")
    super.onCreate(savedInstanceState)
    setContentView(R.layout.ar_preview_activity)

    if (intent.hasExtra("com.reactnativetokkanar.filepath")) {
//      filePath = intent.getStringExtra("com.reactnativetokkanar.filepath") ?: ""
      filePath = intent.extras?.getString("com.reactnativetokkanar.filepath") ?: ""
      Log.d(tagName, "FilePath: $filePath")
    } else {
      Log.d(tagName, "No data found in intent")
    }

    if (filePath.isNotEmpty()) {
      val imageRenderer = ImageRenderer(filePath)

      surfaceView = findViewById(R.id.surfaceview)
      displayRotationHelper = DisplayRotationHelper(/*context=*/ this)

      // Set up renderer.
      surfaceView.preserveEGLContextOnPause = true
      surfaceView.setEGLContextClientVersion(2)
      surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0) // Alpha used for plane blending.
//    surfaceView.setRenderer(this)
      surfaceView.setRenderer(imageRenderer)
//    surfaceView.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY
      surfaceView.renderMode = GLSurfaceView.RENDERMODE_WHEN_DIRTY
      surfaceView.setWillNotDraw(false)
    }

    if (savedInstanceState == null) {
      supportFragmentManager.beginTransaction()
        .replace(R.id.container, ARPreviewFragment.newInstance())
        .commitNow()
    }
  }

  override fun onWindowFocusChanged(hasFocus: Boolean) {
    super.onWindowFocusChanged(hasFocus)
    FullScreenHelper.setFullScreenOnWindowFocusChanged(this, hasFocus)
  }

  fun onBack() {
    val intent = Intent()
    val empty: String? = null
    intent.putExtra("com.reactnativetokkanar.filepath", empty)
    this.setResult(RESULT_OK, intent)
    onBackPressed()
  }
}
