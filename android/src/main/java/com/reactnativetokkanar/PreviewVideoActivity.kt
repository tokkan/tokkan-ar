package com.reactnativetokkanar

import android.opengl.GLSurfaceView
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import com.reactnativetokkanar.rendering.preview.GLVideoRenderer
import com.reactnativetokkanar.ui.main.PreviewVideoFragment


class PreviewVideoActivity : AppCompatActivity() {
  private val tagName = PreviewVideoActivity::class.java.simpleName
  private var glView: GLSurfaceView? = null
  val videoPath = Environment.getExternalStorageDirectory().path + "/Movies/Early.mp4"

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.preview_video_activity)

    glView = findViewById(R.id.surfaceview)
    glView?.setEGLContextClientVersion(2)
    val glVideoRenderer = GLVideoRenderer(this, videoPath) //Create renderer

    glView?.setRenderer(glVideoRenderer) //Set up renderer


    if (savedInstanceState == null) {
      supportFragmentManager.beginTransaction()
        .replace(R.id.container, PreviewVideoFragment.newInstance())
        .commitNow()
    }
  }
}
