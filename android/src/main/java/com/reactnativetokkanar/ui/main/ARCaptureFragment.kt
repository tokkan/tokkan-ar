package com.reactnativetokkanar.ui.main

//import android.widget.Button
////import androidx.compose.Composable
//import androidx.compose.material.Button
//import androidx.compose.material.Text
//import androidx.compose.runtime.Composable
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.platform.ComposeView
//import androidx.compose.ui.platform.setContent
//import androidx.compose.ui.unit.Dp

import android.R.attr.button
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.reactnativetokkanar.ARCaptureActivity
import com.reactnativetokkanar.R
import com.reactnativetokkanar.ui.main.common.buttons.ShutterButton

enum class RecordingType {
  NORMAL,
  BOOMERANG,
  SLOW_MOTION,
}

class ARCaptureFragment : Fragment() {

  companion object {
    fun newInstance() = ARCaptureFragment()
  }

  private val tagName = ARCaptureFragment::class.java.name
  private lateinit var viewModel: ARCaptureViewModel
  private lateinit var backArrowButton: ImageButton
  private lateinit var shutterButton: ShutterButton
  private val maxRecordingTime = 10000L
  private var recordingType = RecordingType.NORMAL
  private var isLongPressed = false
  private var isSwiped = false

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View {
    val rootView = inflater.inflate(R.layout.ar_capture_fragment, container, false)
    backArrowButton = rootView.findViewById(R.id.back_arrow)
//    setupBackArrow(backArrowButton)
    shutterButton = rootView.findViewById(R.id.shutter_button)
    setupShutterButton(shutterButton)
    return rootView

//    val fragmentView = inflater.inflate(
//      R.layout.ar_capture_fragment,
//      container,
//      false)
//    (fragmentView as ViewGroup).setContent {
//      shutterButton()
//    }
//    return fragmentView

//    return ComposeView(requireContext()).apply {
//      setContent {
//        shutterButton()
//      }
//    }
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    viewModel = ViewModelProvider(this).get(ARCaptureViewModel::class.java)
    // TODO: Use the ViewModel
    setupBackArrow(backArrowButton)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
  }

  private fun setupBackArrow(element: ImageButton) {
    element.setOnClickListener {
      Log.d(tagName, "onBackArrow click")
      (activity as ARCaptureActivity).onBack()
    }
  }

  private fun setupShutterButton(element: ShutterButton) {
    element.setOnClickListener {
      Log.d(tagName, "onClick")
      onSnap(element)
    }

    element.setOnLongClickListener {
      Log.d(tagName, "onLongClick")
      onRecord(element, it)
      isLongPressed = true
      true
    }

    // TODO: Look into GestureDetector if it has better/easier implementations
    // of doing different gestures
    // TODO: Returning true is stopping the dispatching of events, but false is not
    // Which means that true could be used if no further dispatching is wanted
    element.setOnTouchListener { v, event ->
      when (event.action) {
        MotionEvent.ACTION_DOWN -> {
          Log.d(tagName, "Motion ACTION_DOWN")
          // TODO: need performClick to get rid of warning, but need
          // to read more about accessibility to handle it properly.
          // Problem here is that ACTION_DOWN will be called on long click as regular click
//          v.performClick()
        }
        MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
          Log.d(tagName, "Motion ACTION_UP, isLongPressed: $isLongPressed")
          if (isLongPressed) {
            isLongPressed = false
            (activity as ARCaptureActivity).onStopRecord()
          }
        }
        MotionEvent.ACTION_MOVE -> {
          // TODO: If move/swiped during recording should keep the recording
          // for the full amount of seconds even when letting go
        }
      }
      false
    }


    element.setOnDragListener { v, event ->
      Log.d(tagName, "onDrag")
      true
    }

    element.setOnGenericMotionListener { v, event ->
      Log.d(tagName, "onMotion")
      true
    }
  }

  private fun onSnap(element: Button) {
    Log.d(tagName, "onSnap")
    (activity as ARCaptureActivity).onSnap()
  }

  private fun onRecord(element: Button, view: View) {
    Log.d(tagName, "ARCaptureFragment onRecord")
    (activity as ARCaptureActivity).onRecord()
//    handler.postDelayed({
    view.handler.postDelayed({
      Log.d(tagName, "onRecord postDelayed")
      if (isLongPressed) {
        isLongPressed = false
        (activity as ARCaptureActivity).onStopRecord()
      }
    }, maxRecordingTime)
  }

//  @Composable
//  fun shutterButton() {
//    Text(text = "hi")
////    Button(
////      onClick = {
////        Log.d(tagName, "Button click")
////      },
//////      modifier = Modifier.foldIn(),
//////      style = OutlinedButtonStyle(
//////        color = Color.WHITE,
//////        contentColor = Color.DKGRAY,
//////        border = Border(Color.WHITE, Dp(4f)),
//////        elevation = Dp(4f)
//////      )
////    ) {
////    }
//  }

//  fun ShutterButtonStyle(
//    shape: Shape = +themeShape { button },
//    contentColor: Color? = +themeColor { primary }
//  )
//  = ButtonStyle(
//    color = Color.TRANSPARENT,
//  shape = shape,
//  paddings = TextButtonPaddings,
//  textStyle = TextStyle(color = contentColor),
//  rippleColor = contentColor
//  )

//  @Composable
//  fun ShutterButton() {
//    Button(
//      text: String,
//      onClick: (() -> Unit)? = null,
//    ) {
////      Log.d(tagName, "ShutterButton")
//      Text("")
//    }
//  }
}
