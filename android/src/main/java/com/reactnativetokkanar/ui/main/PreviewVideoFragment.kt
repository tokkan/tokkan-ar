package com.reactnativetokkanar.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reactnativetokkanar.R

class PreviewVideoFragment : Fragment() {

    companion object {
        fun newInstance() = PreviewVideoFragment()
    }

    private lateinit var viewModel: PreviewVideoViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.preview_video_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PreviewVideoViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
