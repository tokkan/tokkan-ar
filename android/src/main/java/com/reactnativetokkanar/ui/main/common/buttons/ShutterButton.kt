package com.reactnativetokkanar.ui.main.common.buttons

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton

open class ShutterButton @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyleAttr: Int = 0
) : AppCompatButton(context, attrs, defStyleAttr) {

  override fun performClick(): Boolean {
    return super.performClick()
  }
}
