package com.reactnativetokkanar.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.reactnativetokkanar.ARPreviewActivity
import com.reactnativetokkanar.R

class ARPreviewFragment : Fragment() {
  private val tagName = ARPreviewFragment::class.java.simpleName
  private lateinit var backArrowButton: ImageButton

  companion object {
    fun newInstance() = ARPreviewFragment()
  }

  private lateinit var viewModel: ARPreviewViewModel

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View {
    val rootView = inflater.inflate(R.layout.ar_preview_fragment, container, false)
    backArrowButton = rootView.findViewById(R.id.back_arrow)
    return inflater.inflate(R.layout.ar_preview_fragment, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    viewModel = ViewModelProvider(this).get(ARPreviewViewModel::class.java)
    // TODO: Use the ViewModel
    setupBackArrow(backArrowButton)
  }

  private fun setupBackArrow(element: ImageButton) {
    element.setOnClickListener {
      Log.d(tagName, "onBackArrow click")
      (activity as ARPreviewActivity).onBack()
    }
  }
}
