package com.reactnativetokkanar

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.facebook.react.bridge.*

class TokkanArModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
  companion object {
    private val tagName = TokkanArModule::class.java.name
    private lateinit var activityResultPromise: Promise
    private var callback: Callback? = null
    private const val activityRequestCode = 1
    private val errorActivityDoesNotExist = "E_ACTIVITY_DOES_NOT_EXIST"

    private enum class ActivityResultCodes {
      DOES_NOT_EXIST,
      CANCELLED,
      FAILED_TO_SHOW,
      NO_IMAGE_DATA,
    }
  }

  override fun getName(): String {
    return "TokkanAr"
  }

  private val activityEventListener: ActivityEventListener = object : BaseActivityEventListener() {
    override fun onActivityResult(activity: Activity, requestCode: Int, resultCode: Int, intent: Intent) {
      Log.d(tagName, "onActivityResult")
      Log.d(tagName, "Codes: $requestCode $resultCode")
      Log.d(tagName, "Activity OK code: ${Activity.RESULT_OK}")
      Log.d(tagName, "Is promise null: $activityResultPromise")
//      if (requestCode != activityRequestCode || activityResultPromise == null) {
      if (requestCode != activityRequestCode) {
        Log.d(tagName, "Request code was invalid: $requestCode")
        return
      }

//      if (activityResultPromise != null) when (resultCode) {
      when (resultCode) {
        Activity.RESULT_CANCELED -> {
          Log.d(tagName, "Activity cancelled")
          activityResultPromise.resolve("")
        }
        Activity.RESULT_OK -> {
          Log.d(tagName, "RESULT OK for activity")
//          var data: ByteArray? = null
          var data: String? = null
          val uri = intent.data
          Log.d(tagName, uri.toString())
          if (intent.hasExtra("com.reactnativetokkanar.filepath")) {
//            data = intent.extras?.getByteArray("com.reactnativetokkanar.buffer")
//            data = intent.extras?.getString("com.reactnativetokkanar.filename", "")
            data = intent.extras?.getString("com.reactnativetokkanar.filepath")
            Log.d(tagName, "Has putExtra")
            Log.d(tagName, data)
          }

//          if (uri == null) {
          if (data == null) {
            activityResultPromise.reject("E_NO_DATA_FOUND", "No image data found")
          } else {
//            activityResultPromise.resolve(uri.toString())
            activityResultPromise.resolve(data)
          }
        }
        else -> {
          Log.d(tagName, "Else when in resultCode match")
//          activityResultPromise!!.resolve("BLEH")
          activityResultPromise.reject("E_FAILURE_IN_ACTIVITY", "Errors in activity")
//          activityResultPromise = null
        }
      }
    }
  }

  init {
    reactContext.addActivityEventListener(activityEventListener)
  }

  // Example method
  // See https://facebook.github.io/react-native/docs/native-modules-android
  @ReactMethod
  fun multiply(a: Int, b: Int, promise: Promise) {
    promise.resolve(a * b)
  }

  @ReactMethod
  fun show(promise: Promise) {
    val activity = currentActivity
    if (activity != null) {
      activityResultPromise = promise
      val intent = Intent(activity, ARCaptureActivity::class.java)
//      val intent = Intent(activity, PreviewVideoActivity::class.java)
//      val intent = Intent(activity, SharedCameraActivity::class.java)
      val packageManager = reactApplicationContext.packageManager
      if (intent.resolveActivity(packageManager) != null) {
        Log.d("STARTUP", "Intent exists, starting activity")
//        activity.startActivity(intent)
        activity.startActivityForResult(intent, activityRequestCode)
      } else {
        Log.d("STARTUP", "Intent couldn't be resolved")
      }
    }
  }

  @ReactMethod
  fun snap() {
  }
}
