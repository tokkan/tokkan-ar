package com.reactnativetokkanar.effects

import android.media.effect.Effect
import android.media.effect.EffectContext
import android.media.effect.EffectFactory
import android.util.Log

enum class ImageEffectsTypes {
  GRAYSCALE,
  DOCUMENTARY,
  BRIGHTNESS,
}

class ImageEffects(fxContext: EffectContext) {
  private val tagName = ImageEffects::class.java.simpleName

  companion object {
    lateinit var effectContext: EffectContext
    var effect: Effect? = null
  }

  init {
    effectContext = fxContext
  }

  fun applyEffect(types: ImageEffectsTypes, textures: IntArray, width: Int, height: Int): IntArray {
    Log.d(tagName, "Applying effect")
    return when (types) {
      ImageEffectsTypes.GRAYSCALE -> grayScaleEffect(textures, width, height)
      ImageEffectsTypes.BRIGHTNESS -> brightnessEffect(textures, width, height)
      ImageEffectsTypes.DOCUMENTARY -> documentaryEffect(textures, width, height)
    }
  }

  fun releaseEffect() {
    effect?.release()
  }

  private fun grayScaleEffect(textures: IntArray, width: Int, height: Int): IntArray {
    Log.d(tagName, "Applying grayscale effect")
    val factory = effectContext.factory
    effect = factory.createEffect(EffectFactory.EFFECT_GRAYSCALE)
    effect?.apply(textures[0], width, height, textures[1])
    return textures
  }

  private fun documentaryEffect(textures: IntArray, width: Int, height: Int): IntArray {
    val factory = effectContext.factory
    effect = factory.createEffect(EffectFactory.EFFECT_DOCUMENTARY)
    effect?.apply(textures[0], width, height, textures[1])
    return textures
  }

  // TODO: Add args for effect.setParameter
  private fun brightnessEffect(textures: IntArray, width: Int, height: Int): IntArray {
    val factory = effectContext.factory
    effect = factory.createEffect(EffectFactory.EFFECT_BRIGHTNESS)
    effect?.setParameter("brightness", 2f)
    effect?.apply(textures[0], width, height, textures[1])
    return textures
  }
}
